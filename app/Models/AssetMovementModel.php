<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AssetMovementModel extends Model
{
    protected $table = "ql_t_asset_mov_h";
    protected $fillable = [
        't_asset_move_no',
        't_asset_move_date',
        't_asset_mov_from_location_id',
        't_asset_mov_to_location_id',
        't_asset_mov_respon,',
        't_asset_mov_method',
        'created_by',
        'updated_by',
        'created_at',
        'updated_at'
    ];
    protected $primaryKey = "t_asset_mov_h_id";
}

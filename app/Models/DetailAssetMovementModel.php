<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DetailAssetMovementModel extends Model
{
    protected $table = "ql_t_asset_mov_d";
    protected $fillable = [
        't_asset_move_h_id',
        'm_item_id',
        'created_by',
        'updated_by',
        'created_at',
        'updated_at'
    ];
    protected $primaryKey = "t_asset_mov_d_id";
}

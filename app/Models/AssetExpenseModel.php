<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AssetExpenseModel extends Model
{
    protected $table = "ql_t_asset_expense_h";
    protected $fillable = [
        't_asset_expense_no',
        't_asset_expense_date',
        'm_item_id',
        'm_location_id',
        't_asset_expense_h_note',
        'created_at',
        'updated_at',
        'created_by',
        'edited_by'
    ];
    protected $primaryKey = "t_asset_expense_h_id";
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DetailAssetExpenseModel extends Model
{
    protected $table = "ql_t_asset_expense_d";
    protected $fillable = [
        't_expense_h_id',
        'm_account_id',
        't_asset_expense_d_amt',
        't_asset_expense_d_note',
        'created_at',
        'updated_at',
        'created_by',
        'updated_by'
    ];
    protected $primaryKey = "t_asset_expense_d_id";
}

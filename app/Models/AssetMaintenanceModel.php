<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AssetMaintenanceModel extends Model
{
    protected $table = 'ql_t_asset_main_h';

    protected $fillable = [
        't_asset_main_no',
        'm_item_id',
        'm_location_id',
        't_asset_main_h_status',
        'created_by',
        'updated_by',
        'created_at',
        'updated_at'];
    protected $primaryKey = 't_asset_main_h_id';
}

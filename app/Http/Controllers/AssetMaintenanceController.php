<?php

namespace App\Http\Controllers;

use App\Models\AssetMaintenanceModel;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AssetMaintenanceController extends Controller
{
    //
    public function index(){
        $asset2 = DB::table('ql_t_asset_main_h')
        ->join('ql_m_item','ql_m_item.m_item_id','ql_t_asset_main_h.m_item_id')
        ->join('ql_m_location','ql_m_location.m_location_id','ql_t_asset_main_h.m_location_id')
        ->select('ql_t_asset_main_h.*','ql_m_item.m_item_code','ql_m_location.m_location_desc')
        ->orderBy('ql_t_asset_main_h.created_at','DESC')->get();
        return view('master.asset_maintenance.index',compact('asset2'));
    }

    public function create(){
        $barang = DB::table('ql_m_item')->select('m_item_id','m_item_code')->get();
        $location = DB::table('ql_m_location')->select('m_location_id','m_location_desc')->get();
        // $count = DB::table('ql_t_asset_main_h')->count();
        // $count = $count + 1;
        return view('master.asset_maintenance.create',compact('barang','location'));
    }

    public function store(Request $request){
        $this->validate($request, [
            't_asset_main_no'=> 'required',
            'm_item_id' => 'required',
            'm_location_id' => 'required',
            't_asset_main_h_status' => 'required',
        ]);
        DB::table('ql_t_asset_main_h')
            ->insert([
                't_asset_main_no' => $request->t_asset_main_no,
                'm_item_id' => $request->m_item_id,
                'm_location_id' => $request->m_location_id,
                't_asset_main_h_status' => $request->t_asset_main_h_status,
                'created_by' => Auth::user()->name,
                'updated_by' => Auth::user()->name,
                'updated_at' => date('Y-m-d H:i:s'),
        ]);
        $data = DB::table('ql_t_asset_main_h')->orderBy('created_at','desc')->first();
        return response()->json($data);
    }

    public function storeDetail(Request $request){
        $this->validate($request, [
            't_asset_main_h_id'=> 'required',
            't_asset_main_date_plan' => 'required',
            't_asset_main_type' => 'required',
            't_asset_main_date_real' => 'required',
            't_asset_main_d_status' => 'required',
            't_asset_main_d_respon' => 'required',
        ]);
        DB::table('ql_t_asset_main_d')
            ->insert([
                't_asset_main_h_id'=> $request->t_asset_main_h_id,
                't_asset_main_date_plan' => $request->t_asset_main_date_plan,
                't_asset_main_type' => $request->t_asset_main_type,
                't_asset_main_date_real' => $request->t_asset_main_date_real,
                't_asset_main_note_plan' => $request->t_asset_main_note_plan,
                't_asset_main_note_real' => $request->t_asset_main_note_real,
                't_asset_main_d_status' => $request->t_asset_main_d_status,
                't_asset_main_d_respon' => $request->t_asset_main_d_respon,
                'created_by' => Auth::user()->name,
                'updated_by' => Auth::user()->name,
                'updated_at' => date('Y-m-d H:i:s'),
        ]);
    }

    // public function edit($id){

    // }

     public function edit($id){
        $data = [
            'barang' => DB::table('ql_m_item')->select('m_item_id','m_item_code')->get(),
            'location' => DB::table('ql_m_location')->select('m_location_id','m_location_desc')->get(),
            'asset' => DB::table('ql_t_asset_main_h')->where('t_asset_main_h_id','=',$id)->first(),
            'detail' => DB::table('ql_t_asset_main_d')->where('t_asset_main_h_id','=',$id)->orderBy('t_asset_main_d_id','ASC')->get(),
            
        ];
        // dd($data['asset']->t_asset_dep_value*$data['asset']->t_asset_dep_month);
        return view('master.asset_maintenance.update',$data);
    }

    public function editdetail($id){
        $data = [
            'barang' => DB::table('ql_m_item')->select('m_item_id','m_item_code')->get(),
            'location' => DB::table('ql_m_location')->select('m_location_id','m_location_desc')->get(),
            'asset' => DB::table('ql_t_asset_main_h')->where('t_asset_main_h_id','=',$id)->get(),
            'detail' => DB::table('ql_t_asset_main_d')->where('t_asset_main_h_id','=',$id)->first(),
            
        ];
        // dd($data['asset']->t_asset_dep_value*$data['asset']->t_asset_dep_month);
        return view('master.asset_maintenance.update',$data);
    }

    public function update($id,Request $request){
        $this->validate($request, [
            't_asset_main_no'=> 'required',
            'm_item_id' => 'required',
            'm_location_id' => 'required',
            't_asset_main_h_status' => 'required',
        ]);
        DB::table('ql_t_asset_main_h')
            ->where('t_asset_main_h_id', '=', $id)
            ->update([
                't_asset_main_no' => $request->t_asset_main_no,
                'm_item_id' => $request->m_item_id,
                'm_location_id' => $request->m_location_id,
                't_asset_main_h_status' => $request->t_asset_main_h_status,
                'created_by' => Auth::user()->name,
                'updated_by' => Auth::user()->name,
                'updated_at' => date('Y-m-d H:i:s'),
        ]);
        return redirect('asset_maintenance');
    }

    public function updateDetail($id,Request $request){
        // dd($request);
        $this->validate($request, [
            't_asset_main_h_id'=> 'required',
            't_asset_main_date_plan' => 'required',
            't_asset_main_type' => 'required',
            't_asset_main_note_plan' => 'required',
            't_asset_main_date_real' => 'required',
            't_asset_main_note_real' => 'required',
            't_asset_main_d_status' => 'required',
            't_asset_main_d_respon' => 'required',
        ]);
        DB::table('ql_t_asset_main_d')
            ->where('t_asset_main_d_id', '=', $id)  
            ->update([
                't_asset_main_h_id'=> $request->t_asset_main_h_id,
                't_asset_main_date_plan' => $request->t_asset_main_date_plan,
                't_asset_main_type' => $request->t_asset_main_type,
                't_asset_main_note_plan' => $request->t_asset_main_note_plan,
                't_asset_main_date_real' => $request->t_asset_main_date_real,
                't_asset_main_note_real' => $request->t_asset_main_note_real,
                't_asset_main_d_status' => $request->t_asset_main_d_status,
                't_asset_main_d_respon' => $request->t_asset_main_d_respon,
                'created_by' => Auth::user()->name,
                'updated_by' => Auth::user()->name,
                'updated_at' => date('Y-m-d H:i:s'),
        ]);
        return response()->json("berhasil");
    }

     public function destroy($id){
        DB::table('ql_t_asset_main_h')->where('t_asset_main_h_id',$id)->delete();
        DB::table('ql_t_asset_main_d')->where('t_asset_main_d_id',$id)->delete();
        return redirect('asset_maintenance');
    }

    public function destroydetail($id){
        DB::table('ql_t_asset_main_d')->where('t_asset_main_d_id',$id)->delete();
        // $data = DB::table('ql_t_asset_main_d')->orderBy('created_at','desc')->first();
        // return response()->json($data);
    }
    public function getdetail($id){
        $data = DB::table('ql_t_asset_main_d')->where('t_asset_main_d_id',$id)->first();
        return response()->json($data);
    }
}

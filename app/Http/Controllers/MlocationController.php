<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class MlocationController extends Controller
{
    //
    public function index(){
    	$location = DB::table('ql_m_location')->orderBy('created_at','DESC')->get();
    	return view('master.location_master.index',compact('location'));
    }

    public function create(){
    	return view('master.location_master.create');
    }

    public function store(Request $request){
    	$this->validate($request, [
            'm_location_group'=> 'required',
            'm_divisi_id' => 'required',
            'm_departemen_id' => 'required',
            'm_location_geotag_x' => 'required',
            'm_location_geotag_y' => 'required',
            'm_location_desc' => 'required',
            'm_location_picture' => 'required|mimes:jpg,bmp,png,jpeg',
        ]);
        $file = Request()->m_location_picture;
        $location = DB::table('ql_m_location')->orderBy('created_at','DESC')->first();
        if ($location) {
            $fileName = 'location'.($location->m_location_id+1).'.'.$file->extension();
        }else{
            $fileName = 'location1.'.$file->extension();
        }
        $file->move(public_path('img/location'),$fileName);
        DB::table('ql_m_location')
            ->insert([
	            'm_location_group'=> $request->m_location_group,
	            'm_divisi_id' => $request->m_divisi_id,
	            'm_departemen_id' => $request->m_departemen_id,
	            'm_location_geotag_x' => $request->m_location_geotag_x,
	            'm_location_geotag_y' => $request->m_location_geotag_y,
	            'm_location_desc' => $request->m_location_desc,
	            'm_location_picture' => $fileName,
            	'created_by' => Auth::user()->name,
            	'updated_by' => Auth::user()->name,
            	'updated_at' => date('Y-m-d H:i:s'),
        ]);
        return redirect('location_master');
    }

    public function destroy($id){
    	$nama = DB::table('ql_m_location')->where('m_location_id',$id)->first();
        unlink(public_path('img/location').'/'. $nama->m_location_picture);
    	DB::table('ql_m_location')->where('m_location_id',$id)->delete();
    	return redirect('location_master');
    }

    public function edit($id){
    	$data = DB::table('ql_m_location')->where('m_location_id',$id)->first();
    	return view('master.location_master.update',compact('data'));
    }

    public function update($id,Request $request){
    	$this->validate($request, [
            'm_location_group'=> 'required',
            'm_divisi_id' => 'required',
            'm_departemen_id' => 'required',
            'm_location_geotag_x' => 'required',
            'm_location_geotag_y' => 'required',
            'm_location_desc' => 'required',
        ]);
        $nama = DB::table('ql_m_location')->where('m_location_id',$id)->first();
        $fileName = $nama->m_location_picture;
        if ($request->m_location_picture != "") {
            unlink(public_path('img/location').'/'. $fileName);
            $file = Request()->m_location_picture;
            $location = DB::table('ql_m_location')->where('m_location_id',$id)->first();
            if ($location) {
                $fileName = 'location'.($location->m_location_id).'.'.$file->extension();
            }else{
                $fileName = 'location1.'.$file->extension();
            }
            $file->move(public_path('img/location'),$fileName);
        }
        DB::table('ql_m_location')
            ->where('m_location_id', '=', $id)
            ->update([
	            'm_location_group'=> $request->m_location_group,
	            'm_divisi_id' => $request->m_divisi_id,
	            'm_departemen_id' => $request->m_departemen_id,
	            'm_location_geotag_x' => $request->m_location_geotag_x,
	            'm_location_geotag_y' => $request->m_location_geotag_y,
	            'm_location_desc' => $request->m_location_desc,
	            'm_location_picture' => $fileName,
            	'updated_by' => Auth::user()->name,
            	'updated_at' => date('Y-m-d H:i:s'),
        ]);
        return redirect('location_master');
    	
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class MbarangController extends Controller
{
    public function index(){
    	$barang = DB::table('ql_m_item')->orderBy('created_at','DESC')->get();
    	return view('master.barang.index',compact('barang'));
    }
    public function create(){
        $data = DB::table('ql_m_item')->orderBy('created_at','DESC')->first();
        if ($data) {
            $barang = substr($data->m_item_code,13,3) + 1;
        }else{
            $barang = 1;
        }
        $location = DB::table('ql_m_location')->select('m_location_id','m_location_desc')->orderBy('created_at','DESC')->get();
    	return view('master.barang.create',compact('barang','location'));
    }
    public function store(Request $request){
    	// dd(Auth::user()->name);
    	$this->validate($request, [
            'm_company_id'=> 'required',
            'm_divisi_id' => 'required',
            'm_location_id' => 'required',
            'm_item_warranty_type' => 'required',
            'm_item_warranty_expire' => 'required',
            'm_item_warranty_desc' => 'required',
            'm_item_maintenance_plan_type' => 'required',
            'm_item_maintenance_plan' => 'required',
            'm_item_cat1' => 'required',
            'm_item_cat2' => 'required',
            'm_item_cat3' => 'required',
            'm_item_cat4' => 'required',
            'm_item_code' => 'required',
            'm_item_type' => 'required',
            'm_item_flag' => 'required',
            'm_item_unit_id' => 'required',
            'picture' => 'required|mimes:jpg,bmp,png,jpeg',
        ]);
        $file = Request()->picture;
        $barang = DB::table('ql_m_item')->orderBy('created_at','DESC')->first();
        if ($barang) {
            $fileName = 'barang'.($barang->m_item_id+1).'.'.$file->extension();
        }else{
            $fileName = 'barang1.'.$file->extension();
        }
        $file->move(public_path('img/barang'),$fileName);
        DB::table('ql_m_item')
            ->insert([
            	'm_item_code' => $request->m_item_code,
            	'm_item_desc' => $request->m_item_desc,
            	'm_company_id' => $request->m_company_id,
            	'm_divisi_id' => $request->m_divisi_id,
            	'm_departemen_id' => $request->m_departemen_id,
            	'm_location_id' => $request->m_location_id,
            	'm_item_unit_id' => $request->m_item_unit_id,
            	'm_item_cat1' => $request->m_item_cat1,
            	'm_item_cat2' => $request->m_item_cat2,
            	'm_item_cat3' => $request->m_item_cat3,
            	'm_item_cat4' => $request->m_item_cat4,
            	'm_item_type' => $request->m_item_type,
            	'm_item_warranty_type' => $request->m_item_warranty_type,
            	'm_item_warranty_expire' => $request->m_item_warranty_expire,
            	'm_item_warranty_desc' => $request->m_item_warranty_desc,
            	'm_item_maintenance_plan_type' => $request->m_item_maintenance_plan_type,
            	'm_item_maintenance_plan' => $request->m_item_maintenance_plan,
            	'm_item_flag' => $request->m_item_flag,
            	'picture' => $fileName,
            	'created_by' => Auth::user()->name,
            	'updated_by' => Auth::user()->name,
            	'updated_at' => date('Y-m-d H:i:s'),
        ]);
    	return redirect('barang');
    }
    public function destroy($id){

        $nama = DB::table('ql_m_item')->where('m_item_id',$id)->first();
        unlink(public_path('img/barang').'/'. $nama->picture);
    	DB::table('ql_m_item')->where('m_item_id',$id)->delete();
    	return redirect('barang');
    }
    public function edit($id){
        $bar = DB::table('ql_m_item')->where('m_item_id','=',$id)->first();
        $data = [
            'barang' => DB::table('ql_m_item')->where('m_item_id','=',$id)->first(),
            'counter' => substr($bar->m_item_code,13,3),
            'location' => DB::table('ql_m_location')->select('m_location_id','m_location_desc')->orderBy('created_at','DESC')->get(),
        ];
        return view('master.barang.update',$data);
    }
    public function update($id,Request $request){
        $this->validate($request, [
            'm_company_id'=> 'required',
            'm_divisi_id' => 'required',
            'm_location_id' => 'required',
            'm_item_warranty_type' => 'required',
            'm_item_warranty_expire' => 'required',
            'm_item_warranty_desc' => 'required',
            'm_item_maintenance_plan_type' => 'required',
            'm_item_maintenance_plan' => 'required',
            'm_item_cat1' => 'required',
            'm_item_cat2' => 'required',
            'm_item_cat3' => 'required',
            'm_item_cat4' => 'required',
            'm_item_code' => 'required',
            'm_item_type' => 'required',
            'm_item_flag' => 'required',
            'm_item_unit_id' => 'required',
        ]);
        $nama = DB::table('ql_m_item')->where('m_item_id',$id)->first();
        $fileName = $nama->picture;
        if ($request->picture != "") {
            unlink(public_path('img/barang').'/'. $fileName);
            $file = Request()->picture;
            $barang = DB::table('ql_m_item')->where('m_item_id',$id)->first();
            if ($barang) {
                $fileName = 'barang'.($barang->m_item_id).'.'.$file->extension();
            }else{
                $fileName = 'barang1.'.$file->extension();
            }
            $file->move(public_path('img/barang'),$fileName);
        }
        DB::table('ql_m_item')
            ->where('m_item_id', '=', $id)
            ->update([
                'm_item_code' => $request->m_item_code,
                'm_item_desc' => $request->m_item_desc,
                'm_company_id' => $request->m_company_id,
                'm_divisi_id' => $request->m_divisi_id,
                'm_departemen_id' => $request->m_departemen_id,
                'm_location_id' => $request->m_location_id,
                'm_item_unit_id' => $request->m_item_unit_id,
                'm_item_cat1' => $request->m_item_cat1,
                'm_item_cat2' => $request->m_item_cat2,
                'm_item_cat3' => $request->m_item_cat3,
                'm_item_cat4' => $request->m_item_cat4,
                'm_item_type' => $request->m_item_type,
                'm_item_warranty_type' => $request->m_item_warranty_type,
                'm_item_warranty_expire' => $request->m_item_warranty_expire,
                'm_item_warranty_desc' => $request->m_item_warranty_desc,
                'm_item_maintenance_plan_type' => $request->m_item_maintenance_plan_type,
                'm_item_maintenance_plan' => $request->m_item_maintenance_plan,
                'm_item_flag' => $request->m_item_flag,
                'picture' => $fileName,
                'updated_by' => Auth::user()->name,
                'updated_at' => date('Y-m-d H:i:s'),
        ]);
        return redirect('barang');
    }
}

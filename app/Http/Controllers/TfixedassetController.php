<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class TfixedassetController extends Controller
{
    //
    public function index(){
    	$asset = DB::table('ql_t_asset_confirm_h')
    			->leftjoin('ql_m_item','ql_m_item.m_item_id','=','ql_t_asset_confirm_h.m_item_id')
    			->select('ql_t_asset_confirm_h.*','ql_m_item.m_item_code as code')
    			->orderBy('ql_t_asset_confirm_h.created_at','DESC')->get();
    	return view('master.fixed_asset.index',compact('asset'));
    }

    public function create(){
    	$barang = DB::table('ql_m_item')->select('m_item_id','m_item_code')->get();
    	$count = DB::table('ql_t_asset_confirm_h')->count();
    	$count = $count + 1;
    	return view('master.fixed_asset.create',compact('barang','count'));
    }

    public function store(Request $request){
    	$this->validate($request, [
            'm_company_id'=> 'required',
            't_asset_no' => 'required',
            't_asset_date' => 'required',
            'm_item_id' => 'required',
            'm_divisi_id' => 'required',
            't_asset_qty' => 'required',
            't_asset_price' => 'required',
            't_asset_account_id' => 'required',
            't_asset_account_dep_cost_id' => 'required',
            't_asset_dep_month' => 'required',
            't_asset_account_dep_id' => 'required',
            't_asset_dep_value' => 'required',
            't_asset_h_status' => 'required',
        ]);
        DB::table('ql_t_asset_confirm_h')
            ->insert([
	            'm_company_id'=> $request->m_company_id,
	            't_asset_no' => $request->t_asset_no,
	            't_asset_date' => $request->t_asset_date,
	            'm_item_id' => $request->m_item_id,
	            'm_divisi_id' => $request->m_divisi_id,
	            't_asset_qty' => $request->t_asset_qty,
	            't_asset_price' => $request->t_asset_price,
	            't_asset_account_id' => $request->t_asset_account_id,
	            't_asset_account_dep_cost_id' => $request->t_asset_account_dep_cost_id,
	            't_asset_dep_month' => $request->t_asset_dep_month,
	            't_asset_account_dep_id' => $request->t_asset_account_dep_id,
	            't_asset_dep_value' => ((int)$request->t_asset_dep_value*(int)$request->t_asset_dep_month),
	            't_asset_h_status' => $request->t_asset_h_status,
            	'created_by' => Auth::user()->name,
            	'updated_by' => Auth::user()->name,
            	'updated_at' => date('Y-m-d H:i:s'),
        ]);
    $fixedasset = DB::table('ql_t_asset_confirm_h')->orderBy('created_at','DESC')->first();
    $id = $fixedasset->t_asset_h_id;
    $qty = $request->t_asset_qty;
    $val = $request->t_asset_price;
    $dep = $request->t_asset_dep_month;
    $depval = $request->t_asset_dep_value;
    $maxval = $qty * $val;
    $curMonth = $request->curMonth;
    $curMonth = $curMonth + 1;
    $curYear = $request->curYear;
	    if ($depval > 0) {
	      $val = [];
	      	for($i = 0;$i<$dep;$i++){
	        	if ($i < $dep-1) {
			        $maxval = $maxval - $depval;
			        DB::table('ql_t_asset_confirm_d')
			            ->insert([
				            't_asset_h_id'=> $id,
				            't_asset_d_seq' => $i+1,
				            't_asset_d_year' => $curYear,
				            't_asset_d_month' => $curMonth,
				            't_asset_d_periode' => $request->m_divisi_id,
				            't_asset_d_value' => $depval,
				            't_asset_d_accum' => $maxval,
				            't_asset_d_status' => $request->t_asset_h_status,
			            	'created_by' => Auth::user()->name,
			            	'updated_by' => Auth::user()->name,
			            	'updated_at' => date('Y-m-d H:i:s'),
			        ]);
		          	if ($curMonth + 1 == 13) {
		          	  $curMonth = 1;
		          	  $curYear = $curYear + 1;
		          	}else{
		          	  $curMonth = $curMonth + 1;
		          	}
		        }else{
			        DB::table('ql_t_asset_confirm_d')
			            ->insert([
				            't_asset_h_id'=> $id,
				            't_asset_d_seq' => $i+1,
				            't_asset_d_year' => $curYear,
				            't_asset_d_month' => $curMonth,
				            't_asset_d_periode' => $request->m_divisi_id,
				            't_asset_d_value' => $depval,
				            't_asset_d_accum' => 0,
				            't_asset_d_status' => $request->t_asset_h_status,
			            	'created_by' => Auth::user()->name,
			            	'updated_by' => Auth::user()->name,
			            	'updated_at' => date('Y-m-d H:i:s'),
			        ]);
	        	}
	    	}
    	}
    	return redirect('fixed_asset');
	}

    
    public function destroy($id){
    	DB::table('ql_t_asset_confirm_h')->where('t_asset_h_id',$id)->delete();
    	DB::table('ql_t_asset_confirm_d')->where('t_asset_h_id',$id)->delete();
    	return redirect('fixed_asset');
    }
    public function edit($id,$no){
        $data = [
        	'barang' => DB::table('ql_m_item')->select('m_item_id','m_item_code')->get(),
            'asset' => DB::table('ql_t_asset_confirm_h')->where('t_asset_h_id','=',$id)->first(),
            'detail' => DB::table('ql_t_asset_confirm_d')->where('t_asset_h_id','=',$id)->orderBy('t_asset_d_seq','ASC')->get(),
            'no' => $no,
        ];
        // dd($data['asset']->t_asset_dep_value*$data['asset']->t_asset_dep_month);
        return view('master.fixed_asset.update',$data);
    }
    public function update($id,Request $request){
    	$this->validate($request, [
            'm_company_id'=> 'required',
            't_asset_no' => 'required',
            't_asset_date' => 'required',
            'm_item_id' => 'required',
            'm_divisi_id' => 'required',
            't_asset_qty' => 'required',
            't_asset_price' => 'required',
            't_asset_account_id' => 'required',
            't_asset_account_dep_cost_id' => 'required',
            't_asset_dep_month' => 'required',
            't_asset_account_dep_id' => 'required',
            't_asset_dep_value' => 'required',
            't_asset_h_status' => 'required',
        ]);
        DB::table('ql_t_asset_confirm_h')
            ->where('t_asset_h_id', '=', $id)
            ->update([
	            'm_company_id'=> $request->m_company_id,
	            't_asset_no' => $request->t_asset_no,
	            't_asset_date' => $request->t_asset_date,
	            'm_item_id' => $request->m_item_id,
	            'm_divisi_id' => $request->m_divisi_id,
	            't_asset_qty' => $request->t_asset_qty,
	            't_asset_price' => $request->t_asset_price,
	            't_asset_account_id' => $request->t_asset_account_id,
	            't_asset_account_dep_cost_id' => $request->t_asset_account_dep_cost_id,
	            't_asset_dep_month' => $request->t_asset_dep_month,
	            't_asset_account_dep_id' => $request->t_asset_account_dep_id,
	            't_asset_dep_value' => ((int)$request->t_asset_dep_value*(int)$request->t_asset_dep_month),
	            't_asset_h_status' => $request->t_asset_h_status,
            	'updated_by' => Auth::user()->name,
            	'updated_at' => date('Y-m-d H:i:s'),
        ]);
    DB::table('ql_t_asset_confirm_d')->where('t_asset_h_id',$id)->delete();
    $qty = $request->t_asset_qty;
    $val = $request->t_asset_price;
    $dep = $request->t_asset_dep_month;
    $depval = $request->t_asset_dep_value;
    $maxval = $qty * $val;
    $curMonth = $request->curMonth;
    $curMonth = $curMonth + 1;
    $curYear = $request->curYear;
	    if ($depval > 0) {
	      $val = [];
	      	for($i = 0;$i<$dep;$i++){
	        	if ($i < $dep-1) {
			        $maxval = $maxval - $depval;
			        DB::table('ql_t_asset_confirm_d')
			            ->insert([
				            't_asset_h_id'=> $id,
				            't_asset_d_seq' => $i+1,
				            't_asset_d_year' => $curYear,
				            't_asset_d_month' => $curMonth,
				            't_asset_d_periode' => $request->m_divisi_id,
				            't_asset_d_value' => $depval,
				            't_asset_d_accum' => $maxval,
				            't_asset_d_status' => $request->t_asset_h_status,
			            	'created_by' => Auth::user()->name,
			            	'updated_by' => Auth::user()->name,
			            	'updated_at' => date('Y-m-d H:i:s'),
			        ]);
		          	if ($curMonth + 1 == 13) {
		          	  $curMonth = 1;
		          	  $curYear = $curYear + 1;
		          	}else{
		          	  $curMonth = $curMonth + 1;
		          	}
		        }else{
			        DB::table('ql_t_asset_confirm_d')
			            ->insert([
				            't_asset_h_id'=> $id,
				            't_asset_d_seq' => $i+1,
				            't_asset_d_year' => $curYear,
				            't_asset_d_month' => $curMonth,
				            't_asset_d_periode' => $request->m_divisi_id,
				            't_asset_d_value' => $depval,
				            't_asset_d_accum' => 0,
				            't_asset_d_status' => $request->t_asset_h_status,
			            	'created_by' => Auth::user()->name,
			            	'updated_by' => Auth::user()->name,
			            	'updated_at' => date('Y-m-d H:i:s'),
			        ]);
	        	}
	    	}
    	}
    	return redirect('fixed_asset');
    }
}

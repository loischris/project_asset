<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use PDF;

class RassetlocationController extends Controller
{
    public function index(){
    	$asset = DB::table('ql_m_item')->select('m_item_id','m_item_code','m_location_id')->orderBy('created_at','DESC')->get();
    	$location = DB::table('ql_m_location')->select('m_location_id','m_location_desc')->orderBy('created_at','DESC')->get();
    	return view('master.report_asset_location.index',compact('asset','location'));
    }
    public function view($assett,$location,$type){
        $detail = DB::table('ql_t_asset_confirm_d')->orderBy('t_asset_d_id','ASC')->get();
        if  ($type != "yes"){
            if ($assett != "all") {
                $asset = DB::table('ql_t_asset_confirm_h')->where('m_item_id',$assett)->get();
                if ($location != "all") {
                    $barang = DB::table('ql_m_item')->leftjoin('ql_m_location','ql_m_location.m_location_id','ql_m_item.m_location_id')->where('ql_m_item.m_item_id',$assett)->where('ql_m_item.m_location_id',$location)->select('ql_m_item.*','ql_m_location.m_location_desc')->get(); 
                }else{
                    $barang = DB::table('ql_m_item')->leftjoin('ql_m_location','ql_m_location.m_location_id','ql_m_item.m_location_id')->where('ql_m_item.m_item_id',$assett)->select('ql_m_item.*','ql_m_location.m_location_desc')->get();
                }
                $pdf = PDF::loadview('master.report_asset_location.report',compact('barang','asset','detail'));
                $pdf->setPaper('legal','landscape');
                return $pdf->stream();
            }else{
                $asset = DB::table('ql_t_asset_confirm_h')->get();
                if ($location != "all") {
                    $barang = DB::table('ql_m_item')->leftjoin('ql_m_location','ql_m_location.m_location_id','ql_m_item.m_location_id')->select('ql_m_item.*','ql_m_location.m_location_desc')->where('ql_m_item.m_location_id',$location)->orderBy('ql_m_item.created_at','ASC')->get();
                }else{
                    $barang = DB::table('ql_m_item')->leftjoin('ql_m_location','ql_m_location.m_location_id','ql_m_item.m_location_id')->select('ql_m_item.*','ql_m_location.m_location_desc')->orderBy('ql_m_item.created_at','ASC')->get();
                }
                $pdf = PDF::loadview('master.report_asset_location.report',compact('barang','asset','detail'));
                $pdf->setPaper('legal','landscape');
                return $pdf->stream();
            }
        }else {
            if ($location != "all") {
                $location = DB::table('ql_m_location')->where('m_location_id',$location)->get(); 
            }else{
                $location = DB::table('ql_m_location')->get();
            }
            return view('master.report_asset_location.maps',compact('location'));
        }
    }
    public function export($assett,$location,$type){
        $detail = DB::table('ql_t_asset_confirm_d')->orderBy('t_asset_d_id','ASC')->get();
        if ($assett != "all") {
            $asset = DB::table('ql_t_asset_confirm_h')->where('m_item_id',$assett)->get();
            if ($location != "all") {
                $barang = DB::table('ql_m_item')->leftjoin('ql_m_location','ql_m_location.m_location_id','ql_m_item.m_location_id')->where('ql_m_item.m_item_id',$assett)->where('ql_m_item.m_location_id',$location)->select('ql_m_item.*','ql_m_location.m_location_desc')->get();
                }else{
                    $barang = DB::table('ql_m_item')->leftjoin('ql_m_location','ql_m_location.m_location_id','ql_m_item.m_location_id')->where('ql_m_item.m_item_id',$assett)->select('ql_m_item.*','ql_m_location.m_location_desc')->get();
                }
            $pdf = PDF::loadview('master.report_asset_location.report',compact('barang','asset','detail'));
            return $pdf->download("Report asset Location ".date('d-m-Y').'.pdf');
        }else{
            $asset = DB::table('ql_t_asset_confirm_h')->get();
            if ($location != "all") {
                    $barang = DB::table('ql_m_item')->leftjoin('ql_m_location','ql_m_location.m_location_id','ql_m_item.m_location_id')->select('ql_m_item.*','ql_m_location.m_location_desc')->where('ql_m_item.m_location_id',$location)->orderBy('ql_m_item.created_at','ASC')->get();
                }else{
                    $barang = DB::table('ql_m_item')->leftjoin('ql_m_location','ql_m_location.m_location_id','ql_m_item.m_location_id')->select('ql_m_item.*','ql_m_location.m_location_desc')->orderBy('ql_m_item.created_at','ASC')->get();
                }
            $pdf = PDF::loadview('master.report_asset_location.report',compact('barang','asset','detail'));
            return $pdf->download("Report asset Location ".date('d-m-Y').'.pdf');
        }
    }
    public function getloc($id){
        $idbar = DB::table('ql_m_item')->where('m_item_id',$id)->first();
        $location = DB::table('ql_m_location')->where('m_location_id',$idbar->m_location_id)->first();
        return response()->json($location);
    }
    public function getset($id){
        if ($id != "all") {
            $barang = DB::table('ql_m_item')->where('m_location_id',$id)->get();            
        }else{
            $barang = DB::table('ql_m_item')->get();
        }
        return response()->json($barang);
    }


    public function test(){
        $location = DB::table('ql_m_location')->orderBy('created_at','desc')->get();
        return view('master.test',compact('location'));
    }
    public function test2($id){
        $location = DB::table('ql_m_location')->where('m_location_id',$id)->first();
        return response()->json($location);
    }
}

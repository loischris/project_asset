<?php

namespace App\Http\Controllers;

use App\Models\AssetMovementModel;
use App\Models\DetailAssetMovementModel;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class AssetMovementController extends Controller
{
    public function index()
    {
        $asset_movement = DB::table('ql_t_asset_mov_h')
            ->join('ql_m_location', 'ql_m_location.m_location_id', 'ql_t_asset_mov_h.t_asset_mov_from_location_id', 'ql_t_asset_mov_h.t_asset_mov_to_location_id')
            /* ->join('ql_m_location', 'ql_m_location.m_location_id', 'ql_t_asset_mov_h.t_asset_mov_to_location_id') */
            ->select('ql_t_asset_mov_h.*', 'ql_m_location.m_location_desc')
            ->orderBy('ql_t_asset_mov_h.created_at', 'DESC')->get();
        return view('master.asset_movement.index', compact('asset_movement'));
    }

    public function create()
    {
        $barang = DB::table('ql_m_item')->select('m_item_id', 'm_item_code')->get();
        $location = DB::table('ql_m_location')->select('m_location_id', 'm_location_desc')->get();
        // $count = DB::table('ql_t_asset_main_h')->count();
        // $count = $count + 1;
        return view('master.asset_movement.create', compact('barang', 'location'));
    }

    public function detail()
    {
        //
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            't_asset_move_no' => 'required',
            't_asset_move_date' => 'required',
            't_asset_mov_from_location_id' => 'required',
            't_asset_mov_to_location_id' => 'required',
            't_asset_mov_respon' => 'required',
            't_asset_mov_method' => 'required'
        ]);
        DB::table('ql_t_asset_mov_h')
            ->insert([
                't_asset_move_no' => $request->t_asset_move_no,
                't_asset_move_date' => $request->t_asset_move_date,
                't_asset_mov_from_location_id' => $request->t_asset_mov_from_location_id,
                't_asset_mov_to_location_id' => $request->t_asset_mov_to_location_id,
                't_asset_mov_respon' => $request->t_asset_mov_respon,
                't_asset_mov_method' => $request->t_asset_mov_method,
                'created_by' => Auth::user()->name,
                'updated_by' => Auth::user()->name,
                'updated_at' => date('Y-m-d H:i:s'),
            ]);
        $data = DB::table('ql_t_asset_mov_h')->orderBy('created_at', 'desc')->first();
        return response()->json($data);
    }

    public function storeDetail(Request $request)
    {
        $this->validate($request, [
            't_asset_move_h_id' => 'required',
            'm_item_id' => 'required'
        ]);
        DB::table('ql_t_asset_mov_d')
            ->insert([
                't_asset_move_h_id' => $request->t_asset_move_h_id,
                'm_item_id' =>  $request->m_item_id,
                'created_by' => Auth::user()->name,
                'updated_by' => Auth::user()->name,
                'updated_at' => date('Y-m-d H:i:s')
            ]);
    }

    public function show(AssetMovementModel $assetMovementModel, DetailAssetMovementModel $detailAssetMovementModel)
    {
        //
    }

    public function edit($id, AssetMovementModel $assetMovementModel, DetailAssetMovementModel $detailAssetMovementModel)
    {
        $data = [
            'barang' => DB::table('ql_m_item')->select('m_item_id', 'm_item_code')->get(),
            'location' => DB::table('ql_m_location')->select('m_location_id', 'm_location_desc')->get(),
            'asset' => DB::table('ql_t_asset_mov_h')->where('t_asset_move_h_id', '=', $id)->first(),
            'detail' => DB::table('ql_t_asset_mov_d')
                        ->join('ql_m_item', 'ql_m_item.m_item_id', 'ql_t_asset_mov_d.m_item_id')
                        ->join('ql_t_asset_mov_h', 'ql_t_asset_mov_h.t_asset_move_h_id', 'ql_t_asset_mov_d.t_asset_move_h_id')
                        ->where('ql_t_asset_mov_d.t_asset_move_h_id', '=', $id)->orderBy('ql_t_asset_mov_d.t_asset_move_h_id', 'ASC')->get(),
        ];
        return view('master.asset_movement.update', $data);
    }

    public function update($id, Request $request, AssetMovementModel $assetMovementModel, DetailAssetMovementModel $detailAssetMovementModel)
    {
        $this->validate($request, [
            't_asset_move_no' => 'required',
            't_asset_move_date' => 'required',
            't_asset_mov_from_location_id' => 'required',
            't_asset_mov_to_location_id' => 'required',
            't_asset_mov_respon' => 'required',
            't_asset_mov_method' => 'required'
        ]);
        DB::table('ql_t_asset_mov_h')
            ->where('t_asset_move_h_id', '=', $id)
            ->update([
                't_asset_move_no' => $request->t_asset_move_no,
                't_asset_move_date' => $request->t_asset_move_date,
                't_asset_mov_from_location_id' => $request->t_asset_mov_from_location_id,
                't_asset_mov_to_location_id' => $request->t_asset_mov_to_location_id,
                't_asset_mov_respon' => $request->t_asset_mov_respon,
                't_asset_mov_method' => $request->t_asset_mov_method,
                'created_by' => Auth::user()->name,
                'updated_by' => Auth::user()->name,
                'updated_at' => date('Y-m-d H:i:s')
            ]);
        return redirect('asset_movement');
    }

    public function updateDetail($id, Request $request)
    {
        $this->validate($request, [
            't_asset_move_h_id' => 'required',
            'm_item_id' => 'required'
        ]);
        DB::table('ql_t_asset_mov_d')
            ->where('t_asset_move_d_id', '=', $id)
            ->update([
                't_asset_move_h_id' => $request->t_asset_move_h_id,
                'm_item_id' =>  $request->m_item_id,
                'created_by' => Auth::user()->name,
                'updated_by' => Auth::user()->name,
                'updated_at' => date('Y-m-d H:i:s')
            ]);
        return response()->json("berhasil");
    }

    public function destroy($id)
    {
        DB::table('ql_t_asset_mov_h')->where('t_asset_move_h_id', $id)->delete();
        DB::table('ql_t_asset_mov_d')->where('t_asset_move_d_id', $id)->delete();
        return redirect('asset_movement');
    }

    public function destroyDetail($id)
    {
        DB::table('ql_t_asset_mov_d')->where('t_asset_move_d_id', $id)->delete();
        $data = DB::table('ql_t_asset_mov_d')->orderBy('created_at', 'desc')->first();
        return response()->json($data);
    }

    public function getDetail($id)
    {
        $data = DB::table('ql_t_asset_mov_d')->where('t_asset_move_d_id', $id)->first();
        return response()->json($data);
    }
}

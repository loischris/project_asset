<?php

namespace App\Http\Controllers;

use App\Models\AssetExpenseModel;
use App\Models\DetailAssetExpenseModel;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class AssetExpenseController extends Controller
{
    public function index()
    {
        $asset_expense = DB::table('ql_t_asset_expense_h')
            ->join('ql_m_item', 'ql_m_item.m_item_id', 'ql_t_asset_expense_h.m_item_id')
            ->join('ql_m_location', 'ql_m_location.m_location_id', 'ql_t_asset_expense_h.m_location_id')
            ->select('ql_t_asset_expense_h.*', 'ql_m_item.m_item_code', 'ql_m_location.m_location_desc')
            ->orderBy('ql_t_asset_expense_h.created_at', 'DESC')->get();
        return view('master.asset_expense.index', compact('asset_expense'));
    }

    public function create()
    {
        $barang = DB::table('ql_m_item')->select('m_item_id', 'm_item_code')->get();
        $location = DB::table('ql_m_location')->select('m_location_id', 'm_location_desc')->get();
        // $count = DB::table('ql_t_asset_main_h')->count();
        // $count = $count + 1;
        return view('master.asset_expense.create', compact('barang', 'location'));
    }

    public function detail()
    {
        //
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            't_asset_expense_no' => 'required',
            't_asset_expense_date' => 'required',
            'm_item_id' => 'required',
            'm_location_id' => 'required',
            't_asset_expense_h_note' => 'required',
        ]);
        DB::table('ql_t_asset_expense_h')
            ->insert([
                't_asset_expense_no' => $request->t_asset_expense_no,
                't_asset_expense_date' => $request->t_asset_expense_date,
                'm_item_id' => $request->m_item_id,
                'm_location_id' => $request->m_location_id,
                't_asset_expense_h_note' => $request->t_asset_expense_h_note,
                'created_by' => Auth::user()->name,
                'updated_by' => Auth::user()->name,
                'updated_at' => date('Y-m-d H:i:s'),
            ]);
        $data = DB::table('ql_t_asset_expense_h')->orderBy('created_at', 'desc')->first();
        return response()->json($data);
    }

    public function storeDetail(Request $request)
    {
        $this->validate($request, [
            't_asset_expense_h_id' => 'required',
            'm_account_id' => 'required',
            't_asset_expense_d_amt' => 'required',
            't_asset_expense_d_note' => 'required',
        ]);
        DB::table('ql_t_asset_expense_d')
            ->insert([
                't_asset_expense_h_id' => $request->t_asset_expense_h_id,
                'm_account_id' => $request->m_account_id,
                't_asset_expense_d_amt' => $request->t_asset_expense_d_amt,
                't_asset_expense_d_note' => $request->t_asset_expense_d_note,
                'created_by' => Auth::user()->name,
                'updated_by' => Auth::user()->name,
                'updated_at' => date('Y-m-d H:i:s'),
            ]);
    }

    public function show(AssetExpenseModel $assetExpenseModel, DetailAssetExpenseModel $DetailAssetExpenseModel)
    {
        //
    }

    public function edit($id, AssetExpenseModel $assetExpenseModel, DetailAssetExpenseModel $DetailAssetExpenseModel)
    {
        $data = [
            'barang' => DB::table('ql_m_item')->select('m_item_id', 'm_item_code')->get(),
            'location' => DB::table('ql_m_location')->select('m_location_id', 'm_location_desc')->get(),
            'asset' => DB::table('ql_t_asset_expense_h')->where('t_asset_expense_h_id', '=', $id)->first(),
            'detail' => DB::table('ql_t_asset_expense_d')->where('t_asset_expense_h_id', '=', $id)->orderBy('t_asset_expense_h_id', 'ASC')->get(),
        ];
        return view('master.asset_expense.update', $data);
    }

    public function update($id, Request $request)
    {
        $this->validate($request, [
            't_asset_expense_no' => 'required',
            't_asset_expense_date' => 'required',
            'm_item_id' => 'required',
            'm_location_id' => 'required',
            't_asset_expense_h_note' => 'required',
        ]);
        DB::table('ql_t_asset_expense_h')
            ->where('t_asset_expense_h_id', '=', $id)
            ->update([
                't_asset_expense_no' => $request->t_asset_expense_no,
                't_asset_expense_date' => $request->t_asset_expense_date,
                'm_item_id' => $request->m_item_id,
                'm_location_id' => $request->m_location_id,
                't_asset_expense_h_note' => $request->t_asset_expense_h_note,
                'created_by' => Auth::user()->name,
                'updated_by' => Auth::user()->name,
                'updated_at' => date('Y-m-d H:i:s'),
            ]);
        return redirect('asset_expense');
    }

    public function updateDetail($id, Request $request)
    {
        $this->validate($request, [
            't_asset_expense_h_id' => 'required',
            'm_account_id' => 'required',
            't_asset_expense_d_amt' => 'required',
            't_asset_expense_d_note' => 'required'
        ]);
        DB::table('ql_t_asset_expense_d')
            ->where('t_asset_expense_d_id', '=', $id)
            ->update([
                't_asset_expense_h_id' => $request->t_asset_expense_h_id,
                'm_account_id' => $request->m_account_id,
                't_asset_expense_d_amt' => $request->t_asset_expense_d_amt,
                't_asset_expense_d_note' => $request->t_asset_expense_d_note,
                'created_by' => Auth::user()->name,
                'updated_by' => Auth::user()->name,
                'updated_at' => date('Y-m-d H:i:s'),
            ]);
        return response()->json("berhasil");
    }

    public function destroy($id)
    {
        DB::table('ql_t_asset_expense_h')->where('t_asset_expense_h_id', $id)->delete();
        DB::table('ql_t_asset_expense_d')->where('t_asset_expense_d_id', $id)->delete();
        return redirect('asset_expense');
    }

    public function destroyDetail($id)
    {
        DB::table('ql_t_asset_expense_d')->where('t_asset_expense_d_id', $id)->delete();
        $data = DB::table('ql_t_asset_expense_d')->orderBy('created_at', 'desc')->first();
        return response()->json($data);
    }

    public function getDetail($id)
    {
        $data = DB::table('ql_t_asset_expense_d')->where('t_asset_expense_d_id', $id)->first();
        return response()->json($data);
    }

    public function getLocation($id){
        $location = DB::table('ql_m_location')->where('m_location_id',$id)->first();
        return response()->json($location);
    }
}

<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\MbarangController;
use App\Http\Controllers\TfixedassetController;
use App\Http\Controllers\MlocationController;
use App\Http\Controllers\AssetExpenseController;
use App\Http\Controllers\AssetMovementController;
use App\Http\Controllers\AssetMaintenanceController;
use App\Http\Controllers\RassetlocationController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();

Route::group(['middleware' => 'auth'], function () {
    Route::view('/home', 'master.dashboard')->name('home');
    Route::view('/dashboard', 'master.dashboard')->name('dashboard');

    // Barang
    Route::get('/barang', [MbarangController::class, 'index']);
    Route::get('/barang/create', [MbarangController::class, 'create']);
    Route::post('/barang/tambah', [MbarangController::class, 'store']);
    Route::get('/barang/delete/{id}', [MbarangController::class, 'destroy']);
    Route::get('/barang/edit/{id}', [MbarangController::class, 'edit']);
    Route::post('/barang/update/{id}', [MbarangController::class, 'update']);

    // Fixed Asset
    Route::get('/fixed_asset', [TfixedassetController::class, 'index']);
    Route::get('/fixed_asset/create', [TfixedassetController::class, 'create']);
    Route::post('/fixed_asset/store', [TfixedassetController::class, 'store']);
    Route::get('/fixed_asset/delete/{id}', [TfixedassetController::class, 'destroy']);
    Route::get('/fixed_asset/edit/{id}/{no}', [TfixedassetController::class, 'edit']);
    Route::post('/fixed_asset/update/{id}', [TfixedassetController::class, 'update']);

    // Location Master
    Route::get('/location_master', [MlocationController::class, 'index']);
    Route::get('/location_master/create', [MlocationController::class, 'create']);
    Route::post('/location_master/store', [MlocationController::class, 'store']);
    Route::get('/location_master/delete/{id}', [MlocationController::class, 'destroy']);
    Route::get('/location_master/edit/{id}', [MlocationController::class, 'edit']);
    Route::post('/location_master/update/{id}', [MlocationController::class, 'update']);

    // Asset Movement
    Route::get('/asset_movement', [AssetMovementController::class, 'index']);
    Route::get('/asset_movement/create', [AssetMovementController::class, 'create']);
    Route::post('/asset_movement/store', [AssetMovementController::class, 'store']);
    Route::post('/asset_movement/storedetail', [AssetMovementController::class, 'storeDetail']);
    Route::get('/asset_movement/delete/{id}', [AssetMovementController::class, 'destroy']);
    Route::get('/asset_movement/update/delete/{id}', [AssetMovementController::class, 'destroyDetail']);
    Route::get('/asset_movement/edit/{id}', [AssetMovementController::class, 'edit']);
    Route::get('/asset_movement/editdetail/{id}', [AssetMovementController::class, 'editDetail']);
    Route::post('/asset_movement/update/{id}', [AssetMovementController::class, 'update']);
    Route::post('/asset_movement/updatedetail/{id}', [AssetMovementController::class, 'updateDetail']);
    Route::get('/asset_movement/getdetail/{id}', [AssetMovementController::class, 'getDetail']);

    // Asset Maintenance
    Route::get('/asset_maintenance', [AssetMaintenanceController::class, 'index']);
    Route::get('/asset_maintenance/create', [AssetMaintenanceController::class, 'create']);
    Route::post('/asset_maintenance/store', [AssetMaintenanceController::class, 'store']);
    Route::post('/asset_maintenance/storedetail', [AssetMaintenanceController::class, 'storeDetail']);
    Route::get('/asset_maintenance/delete/{id}', [AssetMaintenanceController::class, 'destroy']);
    Route::get('/asset_maintenance/update/delete/{id}', [AssetMaintenanceController::class, 'destroydetail']);
    Route::get('/asset_maintenance/edit/{id}', [AssetMaintenanceController::class, 'edit']);
    Route::get('/asset_maintenance/editdetail/{id}', [AssetMaintenanceController::class, 'editdetail']);
    Route::post('/asset_maintenance/update/{id}', [AssetMaintenanceController::class, 'update']);
    Route::post('/asset_maintenance/updateDetail/{id}', [AssetMaintenanceController::class, 'updateDetail']);
    Route::get('/asset_maintenance/getdetail/{id}', [AssetMaintenanceController::class, 'getdetail']);

    // Asset Expense
    Route::get('/asset_expense', [AssetExpenseController::class, 'index']);
    Route::get('/asset_expense/create', [AssetExpenseController::class, 'create']);
    Route::post('/asset_expense/store', [AssetExpenseController::class, 'store']);
    Route::post('/asset_expense/storedetail', [AssetExpenseController::class, 'storeDetail']);
    Route::get('/asset_expense/delete/{id}', [AssetExpenseController::class, 'destroy']);
    Route::get('/asset_expense/update/delete/{id}', [AssetExpenseController::class, 'destroyDetail']);
    Route::get('/asset_expense/edit/{id}', [AssetExpenseController::class, 'edit']);
    Route::get('/asset_expense/editdetail/{id}', [AssetExpenseController::class, 'editDetail']);
    Route::post('/asset_expense/update/{id}', [AssetExpenseController::class, 'update']);
    Route::post('/asset_expense/updatedetail/{id}', [AssetExpenseController::class, 'updateDetail']);
    Route::get('/asset_expense/getdetail/{id}', [AssetExpenseController::class, 'getDetail']);
    Route::get('/asset_expense/getlocation/{id}', [AssetExpenseController::class, 'getLocation']);

    // Report Asset Location
    Route::get('/report_asset_location', [RassetlocationController::class, 'index']);
    Route::get('/report_asset_location/getloc/{id}', [RassetlocationController::class, 'getloc']);
    Route::get('/report_asset_location/getset/{id}', [RassetlocationController::class, 'getset']);
    Route::get('/report_asset_location/view/{asset}/{location}/{type}', [RassetlocationController::class, 'view']);
    Route::get('/report_asset_location/export/{asset}/{location}/{type}', [RassetlocationController::class, 'export']);

    // Testing
    Route::get('/test', [RassetlocationController::class, 'test']);
    Route::get('/test/getloc/{id}', [RassetlocationController::class, 'test2']);
});

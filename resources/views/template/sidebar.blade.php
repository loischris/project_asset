<!-- Sidebar -->
<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="{{url('/dashboard')}}">
        <div class="sidebar-brand-icon rotate-n-15">
            <i class="fas fa-laugh-wink"></i>
        </div>
        <div class="sidebar-brand-text mx-3">Asset Management</div>
    </a>
    <!-- Divider -->
    <hr class="sidebar-divider my-0">

    <!-- Nav Item - Dashboard -->
    <li class="nav-item @yield('nav-item/dashboard/active')">
        <a class="nav-link" href="{{url('/dashboard')}}">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Dashboard</span></a>
        </li>

        <!-- Divider -->
        <hr class="sidebar-divider">

        <!-- Heading -->
        <div class="sidebar-heading">
            Fixed Assets
        </div>
        <!-- Nav Item - Pages Collapse Menu -->
        <li class="nav-item @yield('nav-item/master/active')">
            <a class="@yield('nav-link/master/collapse')" href="#" data-toggle="collapse" data-target="#fa-master-01"
            aria-expanded="@yield('nav-link/master/aria-expanded')" aria-controls="fa-master-01">
            <i class="fas fa-fw fa-book"></i>
            <span>Master</span>
        </a>
        <div id="fa-master-01" class="collapse @yield('collapse/master/show')" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <h6 class="collapse-header">Master</h6>
                <a class="collapse-item @yield('collapse-item/master/barang')" href="{{url('/barang')}}">Barang</a>
                <a class="collapse-item @yield('collapse-item/master/location')" href="{{url('/location_master')}}">Location</a>

            </div>
        </div>
    </li>
    <li class="nav-item @yield('nav-item/transaction/active')">
        <a class="@yield('nav-link/transaction/collapse')" href="#" data-toggle="collapse" data-target="#fa-tra-01"
        aria-expanded="@yield('nav-link/transaction/aria-expanded')" aria-controls="collapseTwo">
        <i class="fas fa-fw fa-archive"></i>
        <span>Transaction</span>
    </a>
    <div id="fa-tra-01" class="collapse @yield('collapse/transaction/show')" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
        <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Transaction</h6>
            <a class="collapse-item @yield('collapse-item/transaction/fixed_asset')" href="{{url('/fixed_asset')}}">Fixed Asset</a>
            <a class="collapse-item @yield('collapse-item/transaction/asset_movement')" href="{{url('/asset_movement')}}">Asset Movement</a>
            <a class="collapse-item @yield('collapse-item/transaction/asset_expense')" href="{{url('/asset_expense')}}">Asset Expense</a>
            <a class="collapse-item @yield('collapse-item/transaction/asset_maintenance')" href="{{url('/asset_maintenance')}}">Asset Maintenance</a>

        </div>
    </div>
</li>
<li class="nav-item @yield('nav-item/report/active')">
    <a class="@yield('nav-link/report/collapse')" href="#" data-toggle="collapse" data-target="#fa-report-01"
    aria-expanded="@yield('nav-link/report/aria-expanded')" aria-controls="collapseTwo">
    <i class="fas fa-fw fa-file-alt"></i>
    <span>Report</span>
</a>
<div id="fa-report-01" class="collapse @yield('collapse/report/show')" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
    <div class="bg-white py-2 collapse-inner rounded">
        <h6 class="collapse-header">Report</h6>
        <a class="collapse-item @yield('collapse-item/report/report')" href="{{url('/report_asset_location')}}">Laporan Asset & Location</a>
    </div>
</div>
</li>

<!-- Divider -->
<hr class="sidebar-divider d-none d-md-block">

<!-- Sidebar Toggler (Sidebar) -->
<div class="text-center d-none d-md-inline">
    <button class="rounded-circle border-0" id="sidebarToggle"></button>
</div>

</ul>
<!-- End of Sidebar -->

<style type="text/css">
.preloader {
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  z-index: 9999;
  background-color: #f4f5f6;
  opacity: 70%;
  display: none;
}
.preloader .loading {
  position: absolute;
  left: 50%;
  top: 50%;
  transform: translate(-50%,-50%);
  font: 14px arial;
}
</style>
<div id="loader" class="preloader">
  <div class="loading">
    <img src="{{url('css/loader.gif')}}" width="400">
    <!-- <p>Harap Tunggu</p> -->
  </div>
</div>

<script>
  function setLoader(time){
    if(time == 0){
      $('#loader').hide()
    }else{
      setTimeout(function(){ $('#loader').fadeIn(); }, 0);
      setTimeout(function(){ $('#loader').fadeOut('slow'); }, time);
    }
  }
</script>
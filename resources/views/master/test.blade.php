@extends('template.app')

@section('content')
<div class="container-fluid">
                    <!-- DataTales Example -->
                    <div class="card shadow mb-4 w-50">
                        <!-- Card Header - Dropdown -->
                                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                                    <h6 class="m-0 font-weight-bold text-primary">contoh</h6>
                                </div>
                        <div class="card-body">
                            <div class="row mb-3">
                              <label class="col-sm-2 col-form-label col-form-label-sm">Location</label>
                              <div class="col-sm-10">
                                <select class="form-control form-control-sm" id="location">
                                    <option value="">Pilih Location</option>
                                    @foreach($location as $loc)
                                        <option value="{{$loc->m_location_id}}">{{$loc->m_location_desc}}</option>
                                    @endforeach
                                    </select>
                              </div>
                            </div>
	                        <div class="row mb-3">
	                          <label class="col-sm-4 col-form-label col-form-label-sm">Location Map Picture</label>
	                          <div class="col-sm-8">
	                                <div id="map"
	                               style="width: 300px; height: 200px"></div>
	                          </div>
	                        </div>
                            
                        </div>
                    </div>

                </div>
@endsection

@section('js')
  <script src=
"https://maps.google.com/maps/api/js?sensor=false">
</script>
<script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
<script>
	$('#location').on('change',function(e){
    	if ($('#location').val() != "") {
    		var id = $('#location').val();
    		var locx = 0;
    		var locy = 0;
    		$.ajax({
                url: "{{ url('test/getloc') }}"+"/"+id,
                type: "GET",
                success: function(data){
                   	locx = data.m_location_geotag_x;
                   	locy = data.m_location_geotag_y;
		    		latt = parseFloat(locx);
			        long = parseFloat(locy);
			        var lattlong = new google.maps.LatLng(latt,long);
			        var OPTions = {
			          center: lattlong,
			          zoom: 14,
			          mapTypeControl: true,
			          navigationControlOptions: {
			            style: google.maps.NavigationControlStyle.SMALL,
			          },
			        };
			        var mapg = new google.maps.Map(
			          document.getElementById("map"),
			          OPTions
			        );
			        var markerg = new google.maps.Marker({
			          position: lattlong,
			          map: mapg,
			          title: "You are here!",
			        });
                },
            });
    	}
  	});
  	$(document).ready(function () {
        latt = -7.931489;
        long = 113.7904826;
        var lattlong = new google.maps.LatLng(latt,long);
        var OPTions = {
          center: lattlong,
          zoom: 14,
          mapTypeControl: true,
          navigationControlOptions: {
            style: google.maps.NavigationControlStyle.SMALL,
          },
        };
        var mapg = new google.maps.Map(
          document.getElementById("map"),
          OPTions
        );
        var markerg = new google.maps.Marker({
          position: lattlong,
          map: mapg,
          title: "You are here!",
        });
    });
</script>
@endsection
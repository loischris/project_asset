@extends('template.app')

@section('nav-link/master/collapse')
nav-link collapsed
@endsection
@section('nav-link/master/aria-expanded')
false
@endsection

@section('nav-link/report/collapse')
nav-link collapsed
@endsection
@section('nav-link/report/aria-expanded')
false
@endsection

@section('page-title')
Asset Maintenance - Update
@endsection

@section('nav-item/transaction/active')
active
@endsection

@section('nav-link/transaction/collapse')
nav-link
@endsection

@section('nav-link/transaction/aria-expanded')
true
@endsection

@section('collapse/transaction/show')
show
@endsection

@section('collapse-item/transaction/asset_maintenance')
active bg-light
@endsection

@section('content')
<div class="container-fluid">
    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <!-- Card Header - Dropdown -->
        <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
            <h6 class="m-0 font-weight-bold text-primary">Asset Maintenance</h6>
        </div>
        <form method="POST"  enctype="multipart/form-data" id="form_asset_maintenance_update" action="javascript:void(0)" >
            @csrf
            <!-- @csrf -->
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="row mb-3">
                            <label class="col-sm-4 col-form-label col-form-label-sm">Asset No.</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control form-control-sm" value="{{$asset->t_asset_main_no}}" name="t_asset_main_no" id="t_asset_main_no">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="row mb-3">
                            <label class="col-sm-4 col-form-label col-form-label-sm">Item Id</label>
                            <div class="col-sm-8">
                                <div class="input-group">
                                    <select class="form-control form-control-sm" aria-describedby="button-addon2" name="m_item_id">
                                       @foreach($barang as $bar)
                                        <option value="{{$bar->m_item_id}}"
                                            @if($asset->m_item_id == $bar->m_item_id)
                                            selected
                                            @endif
                                            >{{$bar->m_item_code}}</option>
                                            @endforeach
                                        </select>
                                    </select>
                                    <button class="btn btn-outline-secondary btn-sm" type="button" id="button-addon2"><i class="fa fa-search"></i></button>
                                </div>
                            </div>
                            <!--  <div class="col-sm-8">
                                <input type="text" class="form-control form-control-sm" id="">
                            </div> -->
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="row mb-3">
                            <label class="col-sm-4 col-form-label col-form-label-sm">Location</label>
                            <div class="col-sm-8">
                                <select class="form-control form-control-sm" name="m_location_id">
                                    @foreach($location as $loca)
                                     <option value="{{$loca->m_location_id}}"
                                            @if($asset->m_location_id == $loca->m_location_id)
                                            selected
                                            @endif
                                            >{{$loca->m_location_desc}}</option>
                                            @endforeach
                                        </select>
                                </select>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="row mb-3">
                            <label class="col-sm-4 col-form-label col-form-label-sm">Status </label>
                            <div class="col-sm-8">
                                <input type="radio" name="t_asset_main_h_status" id="t_asset_main_h_status1" value="Active"
                                @if($asset->t_asset_main_h_status == 'Active')
                                checked
                                @endif
                                >
                                <label class="form-check-label mr-3" for="t_asset_main_h_status1">
                                    Active
                                </label>
                                <input type="radio" name="t_asset_main_h_status" id="t_asset_main_h_status2" value="Inactive"
                                @if($asset->t_asset_main_h_status == 'Inactive')
                                checked
                                @endif
                                >
                                <label class="form-check-label mr-3" for="t_asset_main_h_status2">
                                    Inactive
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row " id="tomboladd">
                    <div class="col-md-12">
                        <div class="float-left">
                            <button type="submit" class="btn-success d-none d-sm-inline-block btn btn-md shadow-sm mr-3" id="up">Simpan</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        <div class="card-body">
            <div class="row my-4">
                <div class="col-md-12">
                    <h6 class="m-0 font-weight-bold text-primary">Detail Asset Maintenance</h6>
                </div>
            </div>
            <div class="row" id="addNew">
                <div class="col-md-12">
                    <div class="float-left">
                        <button type="button" data-toggle="modal" data-target="#exampleModal" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm mb-2" id="tambahkan">Tambahkan</button>
                    </div>
                </div>
            </div>


            <!-- <div class="modal fade" id="exampleModaledit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Edit Detail Asset Maintenance</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <form method="POST" enctype="multipart/form-data" id="form_detail_asset_maintenance" action="javascript:void(0)" >
                            <div class="modal-body">
                                <div class="form-group">
                                    <label for="recipient-name" class="col-form-label">Asset Maintenance :</label>
                                    <input type="hidden" name="t_asset_main_h_id" id="t_asset_main_h_id" value="{{$asset->t_asset_main_h_id}}">
                                    <input type="text" class="form-control form-control-sm" id="nama" disabled="" value="">
                                </div>
                                <div class="form-group">
                                    <label for="recipient-name" class="col-form-label">Date Plan :</label>
                                    <input type="date" class="form-control form-control-sm" value="" name="t_asset_main_date_plan" id="t_asset_main_date_plan">
                                </div>
                                <div class="form-group">
                                    <label for="recipient-name" class="col-form-label">Type :</label>
                                    <select class="form-control form-control-sm" name="t_asset_main_type" id="t_asset_main_type">
                                        <option value="" selected disabled>Pilih Type</option>
                                        <option value="Routine">Routine</option>
                                        <option value="Special">Special</option>
                                        <option value="Damage">Damage</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="recipient-name" class="col-form-label">Note Plan :</label>
                                    <textarea class="form-control" name="t_asset_main_note_plan" id="t_asset_main_note_plan"></textarea>
                                </div>
                                <div class="form-group">
                                    <label for="recipient-name" class="col-form-label">Date Real :</label>
                                    <input type="date" class="form-control form-control-sm" name="t_asset_main_date_real" id="t_asset_main_date_real">
                                </div>
                                <div class="form-group">
                                    <label for="recipient-name" class="col-form-label">Note Real :</label>
                                    <textarea class="form-control" name="t_asset_main_note_real" id="t_asset_main_note_real"></textarea>
                                </div>
                                <div class="form-group">
                                    <label for="message-text" class="col-form-label">Status:</label>
                                    <div class="col-sm-8">
                                        <input type="radio" name="t_asset_main_d_status" id="t_asset_main_d_status1" checked value="Active">
                                        <label class="form-check-label mr-3" for="t_asset_main_h_status1">
                                            Active
                                        </label>
                                        <input type="radio" name="t_asset_main_d_status" id="t_asset_main_d_status2" value="Inactive">
                                        <label class="form-check-label mr-3" for="t_asset_main_h_status2">
                                            Inactive
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="recipient-name" class="col-form-label">Respon :</label>
                                    <input type="text" class="form-control form-control-sm" name="t_asset_main_d_respon" id="t_asset_main_d_respon">
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button class="btn btn-primary" id="addDetail">Simpan</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div> -->


            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">New Detail Asset Maintenance</h5>
                            <button type="button" class="close" data-dismiss="modal" onclick="clr()" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <form method="POST" enctype="multipart/form-data" id="form_detail_asset_maintenance" action="javascript:void(0)" >
                            <div class="modal-body">
                                <div class="form-group">
                                    <label for="recipient-name" class="col-form-label">Asset Maintenance :</label>
                                    <input type="hidden" name="t_asset_main_d_id" id="t_asset_main_d_id"> 
                                    <input type="hidden" name="t_asset_main_h_id" id="t_asset_main_h_id" value="{{$asset->t_asset_main_h_id}}">
                                    <input type="text" class="form-control form-control-sm" id="nama" disabled="" value="{{$asset->t_asset_main_h_id}}">
                                </div>
                                <div class="form-group">
                                    <label for="recipient-name" class="col-form-label">Date Plan :</label>
                                    <input type="date" class="form-control form-control-sm" name="t_asset_main_date_plan" id="t_asset_main_date_plan">
                                </div>
                                <div class="form-group">
                                    <label for="recipient-name" class="col-form-label">Type :</label>
                                    <select class="form-control form-control-sm" name="t_asset_main_type" id="t_asset_main_type">
                                        <option value="" selected disabled>Pilih Type</option>
                                        <option value="Routine">Routine</option>
                                        <option value="Special">Special</option>
                                        <option value="Damage">Damage</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="recipient-name" class="col-form-label">Note Plan :</label>
                                    <textarea class="form-control" name="t_asset_main_note_plan" id="t_asset_main_note_plan"></textarea>
                                </div>
                                <div class="form-group">
                                    <label for="recipient-name" class="col-form-label">Date Real :</label>
                                    <input type="date" class="form-control form-control-sm" name="t_asset_main_date_real" id="t_asset_main_date_real">
                                </div>
                                <div class="form-group">
                                    <label for="recipient-name" class="col-form-label">Note Real :</label>
                                    <textarea class="form-control" name="t_asset_main_note_real" id="t_asset_main_note_real"></textarea>
                                </div>
                                <div class="form-group">
                                    <label for="message-text" class="col-form-label">Status:</label>
                                    <div class="col-sm-8">
                                        <input type="radio" name="t_asset_main_d_status" id="t_asset_main_d_status1" checked value="Active">
                                        <label class="form-check-label mr-3" for="t_asset_main_h_status1">
                                            Active
                                        </label>
                                        <input type="radio" name="t_asset_main_d_status" id="t_asset_main_d_status2" value="Inactive">
                                        <label class="form-check-label mr-3" for="t_asset_main_h_status2">
                                            Inactive
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="recipient-name" class="col-form-label">Respon :</label>
                                    <input type="text" class="form-control form-control-sm" name="t_asset_main_d_respon" id="t_asset_main_d_respon">
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="clr()">Close</button>
                                <div id="btnadd">
                                <button class="btn btn-primary" id="addDetail">Simpan</button>
                                </div>
                                <div id="btnedit">
                                <button class="btn btn-primary" id="editDetail">Update</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-md-12">
                    <div class="table-responsive">
                        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                            <thead>
                                @php
                                    $no = 1;
                                @endphp
                                <tr>
                                    <th>No</th>
                                    <th>Date Plan</th>
                                    <th>Type</th>
                                    <th>Note Plan</th>
                                    <th>Date Real</th>
                                    <th>Note Real</th>
                                    <th>Status</th>
                                    <th>Respon</th>
                                    <th>Aksi</th>

                                </tr>
                            </thead>
                            <tbody>
                                 @foreach($detail as $det)
                                    <tr>
                                        <td>{{$no++}}</td>
                                        <td>{{ date('Y-m-d',strtotime($det->t_asset_main_date_plan))}}</td>
                                        <td>{{$det->t_asset_main_type}}</td>
                                        <td>{{$det->t_asset_main_note_plan}}</td>
                                        <td>{{date('Y-m-d',strtotime($det->t_asset_main_date_real))}}</td>
                                        <td>{{$det->t_asset_main_note_real}}</td>
                                        <td>{{$det->t_asset_main_d_status}}</td>
                                        <td>{{$det->t_asset_main_d_respon}}</td>
                                        <td>
                                             <div class="row">
                                    <div class="col-md-5">

                                         <button class="btn-danger d-none d-sm-inline-block btn btn-sm shadow-sm mr-3" style="font-size: 20px;" title="hapus"><ion-icon name="trash-outline" onclick="del('{{ $det->t_asset_main_d_id }}')"></ion-icon></button>
                                    </div>

                                    <div class="col-md-5">
                                        <!-- <button type="button" data-toggle="modal" data-target="#exampleModaledit" data-id="{{ $asset->t_asset_main_h_id }}" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm mb-2" onclick="edit('{{ $asset->t_asset_main_h_id }}')">Tambahkan</button> -->


                                        <button class="btn-warning d-none d-sm-inline-block btn btn-sm shadow-sm mr-3" data-toggle="modal" data-target="#exampleModal"  style="font-size: 20px;" title="Edit"><ion-icon name="create-outline" onclick="dit('{{ $det->t_asset_main_d_id }}')"></ion-icon></button>
                                    </div>
                                </div>
                                        </td>
                                    </tr>
                                    @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <br>
            <div class="row float-right">
                <a href="#" class="btn-success d-none d-sm-inline-block btn btn-md shadow-sm mr-3" >Simpan</a>
                <a href="#" class="btn-warning d-none d-sm-inline-block btn btn-md shadow-sm mr-3">Terbitkan</a>
                <a href="{{url('asset_maintenance')}}" class="btn-light d-none d-sm-inline-block btn btn-md shadow-sm">Cancel</a>
            </div>
        </div>
    </div>
</div>
@endsection
@section('js')<!--
<script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script> -->
<script src="https://cdn.ckeditor.com/4.16.2/standard/ckeditor.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfobject/2.2.7/pdfobject.min.js" integrity="sha512-g16L6hyoieygYYZrtuzScNFXrrbJo/lj9+1AYsw+0CYYYZ6lx5J3x9Yyzsm+D37/7jMIGh0fDqdvyYkNWbuYuA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script type="module" src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.esm.js"></script>
<script nomodule src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.js"></script>
<script type="text/javascript">

   

    function del($id){
        $.ajax({
            type:'GET',
            url:"{{url('asset_maintenance/update/delete')}}"+"/"+$id,

            success: function(data) {
                alert('Category Deleted!');
                   location.reload();

            },
            error: function() {
                alert('Error occured');
            }
        })
    }

    // function del($id){
    //     window.location.href="{{url('asset_maintenance/update/delete')}}"+"/"+$id;
    // }
    // function edit($id){
    //     window.location.href="{{url('asset_maintenance/editdetail')}}"+"/"+$id;
    // }
    function dit(id){
        // console.log(id);
        $.ajax({
            type:'GET',
            url: "{{url('/asset_maintenance/getdetail')}}"+"/"+id,
            success: (data) => {
                $('#btnedit').show();
                $('#btnadd').hide();
                var dp = new Date(data.t_asset_main_date_plan);
                if (dp.getDate() < 10) {
                    var dpdate = "0"+dp.getDate();
                }else{
                    var dpdate = dp.getDate();
                }
                dpMonth = dp.getMonth()+1;
                $('#t_asset_main_date_plan').val(dp.getFullYear()+"-"+dpMonth+"-"+dpdate);
                $('#t_asset_main_type').val(data.t_asset_main_type).trigger('change');
                $('#t_asset_main_note_plan').val(data.t_asset_main_note_plan);
                var dr = new Date(data.t_asset_main_date_real);
                if (dr.getDate() < 10) {
                    var drdate = "0"+dr.getDate();
                }else{
                    var drdate = dr.getDate();
                }
                drMonth = dr.getMonth()+1;
                $('#t_asset_main_date_real').val(dr.getFullYear()+"-"+drMonth+"-"+drdate);
                $('#t_asset_main_note_real').val(data.t_asset_main_note_real);
                $('input:radio[name="t_asset_main_d_status"]').filter('[value="'+data.t_asset_main_d_status+'"]').attr('checked', true);
                $('#t_asset_main_d_respon').val(data.t_asset_main_d_respon);
                $('#t_asset_main_d_id').val(data.t_asset_main_d_id);
                // console.log(data);


            },
            error: function(data){
            },
            complete: function() {
            }
        });
    };
    function clr(){
        $('#btnedit').hide();
        $('#btnadd').show();
    }
    $(document).ready(function() {

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $('#btnedit').hide();
        var no = 1;
        $('#addDetail').click(function(){
            var date_plan = $('#t_asset_main_date_plan').val();
            var dp = new Date(date_plan);
            var type = $('#t_asset_main_type').val();
            var note_plan = $('#t_asset_main_note_plan').val();
            var date_real = $('#t_asset_main_date_real').val();
            var dr = new Date(date_real);
            var note_real = $('#t_asset_main_note_real').val();
            var d_status = $("input[name='t_asset_main_d_status']:checked").val();
            var d_respon = $('#t_asset_main_d_respon').val();
            if (date_plan != "" && type != "" && date_real != "" && d_status != "" && d_respon != "") {
                $("#exampleModal .close").click();
                var tableBody = $('table tbody');
                var markup = "";

                markup += `<tr>`;
                markup += `<td>`;
                markup += no++;
                markup += `</td>`;
                markup += `<td>`;
                markup += (dp.getFullYear()+"-"+dp.getMonth()+"-"+dp.getDate());
                markup += `</td>`;
                markup += `<td>`;
                markup +=  type;
                markup += `</td>`;
                markup += `<td>`;
                markup +=  note_plan;
                markup += `</td>`;
                markup += `<td>`;
                markup +=  (dr.getFullYear()+"-"+dr.getMonth()+"-"+dr.getDate());
                markup += `</td>`;
                markup += `<td>`;
                markup +=  note_real;
                markup += `</td>`;
                markup += `<td>`;
                markup +=  d_status;
                markup += `</td>`;
                markup += `<td>`;
                markup +=  d_respon;
                markup += `</td>`;
                markup += '</tr>';

           
                markup += `</tr>`;

                tableBody.append(markup);

                $('#form_detail_asset_maintenance').submit(function(e) {
                    e.preventDefault();
                    var me2 = $(this);
                    if (me2.data('requestRunning')) {
                        return;
                    }
                    me2.data('requestRunning', true);
                    var formDataDetail = new FormData(this);
                    $.ajax({
                        type:'POST',
                        url: "{{ url('/asset_maintenance/storedetail')}}",
                        data: formDataDetail,
                        cache:false,
                        contentType: false,
                        processData: false,
                        success: (data) => {

                        },
                        error: function(data){
                        },
                        complete: function() {
                            me2.data('requestRunning', false);
                        }
                    });
                });
            }
        });
        $('#tambahkan').click(function(){
            $('#t_asset_main_date_plan').val("");
            $('#t_asset_main_type').val("").trigger('change');
            $('#t_asset_main_note_plan').val("");
            $('#t_asset_main_date_real').val("");
            $('#t_asset_main_note_real').val("");
            $('input:radio[name="t_asset_main_d_status"]').filter('[value="Active"]').attr('checked', true);
            $('#t_asset_main_d_respon').val("");
            $('#t_asset_main_d_id').val("");
        });
        $('#editDetail').click(function(){
            var id = $('#t_asset_main_d_id').val();
            // console.log(id);
            var date_plan = $('#t_asset_main_date_plan').val();
            var dp = new Date(date_plan);
            var type = $('#t_asset_main_type').val();
            var note_plan = $('#t_asset_main_note_plan').val();
            var date_real = $('#t_asset_main_date_real').val();
            var dr = new Date(date_real);
            var note_real = $('#t_asset_main_note_real').val();
            var d_status = $("input[name='t_asset_main_d_status']:checked").val();
            var d_respon = $('#t_asset_main_d_respon').val();
            if (date_plan != "" && type != "" && date_real != "" && d_status != "" && d_respon != "") {
                $("#exampleModal .close").click();


                $('#form_detail_asset_maintenance').submit(function(e) {
                    e.preventDefault();
                    var me2 = $(this);
                    if (me2.data('requestRunning')) {
                        return;
                    }
                    me2.data('requestRunning', true);
                    var formDataDetail = new FormData(this);
                    $.ajax({
                        type:'POST',
                        url: "{{ url('/asset_maintenance/updateDetail')}}"+"/"+id,
                        data: formDataDetail,
                        cache:false,
                        contentType: false,
                        processData: false,
                        success: (data) => {
                            alert("succes edit data");
                            location.reload();
                        },
                        error: function(data){

                        },
                        complete: function() {
                            me2.data('requestRunning', false);
                        }
                    });
                });
            }
        });
        $('#up').click(function(){
                $('#form_asset_maintenance_update').submit(function(e) {
                    e.preventDefault();
                    var me1 = $(this);
                    if ( me1.data('requestRunning') ) {
                        return;
                    }
                    me1.data('requestRunning', true);
                    var formData = new FormData(this);
                    // add

                    $.ajax({
                        type:'POST',
                        url: "{{url('/asset_maintenance/update/'.$asset->t_asset_main_h_id)}}",
                        data: formData,
                        cache:false,
                        contentType: false,
                        processData: false,
                        success: (data) => {
                            $('#t_asset_main_h_id').val(data.t_asset_main_h_id);
                            $('#nama').val(data.t_asset_main_no);
                            // $('#addNew').show();
                            // $('#tomboladd').hide();
                        },
                        error: function(data){
                        },
                        complete: function() {
                            me1.data('requestRunning', false);
                        }
                    });
                });
            });
    });
</script>

@endsection

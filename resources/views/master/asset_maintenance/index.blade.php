@extends('template.app')

@section('nav-link/master/collapse')
nav-link collapsed
@endsection
@section('nav-link/master/aria-expanded')
false
@endsection

@section('nav-link/report/collapse')
nav-link collapsed
@endsection
@section('nav-link/report/aria-expanded')
false
@endsection

@section('page-title')
Asset Maintenance - Home
@endsection

@section('nav-item/transaction/active')
active
@endsection

@section('nav-link/transaction/collapse')
nav-link
@endsection

@section('nav-link/transaction/aria-expanded')
true
@endsection

@section('collapse/transaction/show')
show
@endsection

@section('collapse-item/transaction/asset_maintenance')
active bg-light
@endsection

@section('content')
<div class="container-fluid">
    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <!-- Card Header - Dropdown -->
        <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
            <h6 class="m-0 font-weight-bold text-primary">Asset Maintenance</h6>
            <div class="dropdown no-arrow">
                <a href="{{url('/asset_maintenance/create')}}" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-plus fa-sm text-white-50"></i> Add New</a>
            </div>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>Asset No.</th>
                            <th>Item</th>
                            <th>Location</th>
                            <th>Status</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($asset2 as $asset)
                        <tr>
                            <td>{{$asset->t_asset_main_no}}</td>
                            <td>{{$asset->m_item_code}}</td>
                            <td>{{$asset->m_location_desc}}</td>
                            <td>{{$asset->t_asset_main_h_status}}</td>
                            <td>
                                <div class="row">
                                    <div class="col-md-5">

                                         <button class="btn-danger d-none d-sm-inline-block btn btn-sm shadow-sm mr-3" style="font-size: 20px;" title="hapus"><ion-icon name="trash-outline" onclick="del('{{ $asset->t_asset_main_h_id }}')"></ion-icon></button>
                                    </div>

                                    <div class="col-md-5">
                                        <!-- <button class="btn-warning d-none d-sm-inline-block btn btn-sm shadow-sm mr-3" style="font-size: 20px;" title="Edit"><ion-icon name="create-outline" onclick="edit('{{ $asset->m_item_id }}')"></ion-icon></button> -->
                                        <button class="btn-warning d-none d-sm-inline-block btn btn-sm shadow-sm mr-3" style="font-size: 20px;" title="Edit"><ion-icon name="create-outline" onclick="edit('{{ $asset->t_asset_main_h_id }}')"></ion-icon></button>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
@section('js')
<script type="module" src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.esm.js"></script>
<script nomodule src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.js"></script>
<script type="text/javascript">
    function del($id){
        window.location.href="{{url('asset_maintenance/delete')}}"+"/"+$id;
    }
    function edit($id){
        window.location.href="{{url('asset_maintenance/edit')}}"+"/"+$id;
    }
</script>
<script>
    $(document).ready(function() {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $('#up').hide();
    });
</script>
<script>
    function del($id){
        window.location.href="{{url('asset_maintenance/delete')}}"+"/"+$id;
    }
    function edit($id){
        window.location.href="{{url('asset_maintenance/edit')}}"+"/"+$id;
    }
</script>
@endsection

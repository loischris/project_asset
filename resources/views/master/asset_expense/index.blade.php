@extends('template.app')

@section('nav-link/master/collapse')
nav-link collapsed
@endsection
@section('nav-link/master/aria-expanded')
false
@endsection

@section('nav-link/report/collapse')
nav-link collapsed
@endsection
@section('nav-link/report/aria-expanded')
false
@endsection

@section('page-title')
Asset Expense - Home
@endsection

@section('nav-item/transaction/active')
active
@endsection

@section('nav-link/transaction/collapse')
nav-link
@endsection

@section('nav-link/transaction/aria-expanded')
true
@endsection

@section('collapse/transaction/show')
show
@endsection

@section('collapse-item/transaction/asset_expense')
active bg-light
@endsection

@section('content')
<div class="container-fluid">
    <div class="card shadow mb-4">
        <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
            {{-- <div class="dropdown no-arrow d-none d-lg-block d-xl-block">
                <a href="{{url('/dashboard')}}" class="btn btn-sm btn-primary shadow-sm"><i class="fas fa-arrow-left fa-sm text-white-50"></i> Kembali</a>
            </div> --}}
            <h6 class="m-0 font-weight-bold text-primary">Asset Expense</h6>
            {{-- <div class="dropdown no-arrow d-none d-lg-block d-xl-block">
                <a href="{{url('/asset_expense/pdf')}}" class="btn btn-sm btn-danger shadow-sm disabled"><i class="fas fa-download fa-sm text-white-50"></i> Export PDF</a>
                <a href="{{url('/asset_expense/excel')}}" class="btn btn-sm btn-info shadow-sm disabled"><i class="fas fa-table fa-sm text-white-50"></i> Export Excel</a>
                --}}<a href="{{url('/asset_expense/create')}}" class="btn btn-sm btn-primary shadow-sm"><i class="fas fa-plus fa-sm text-white-50"></i> Add New</a>
            {{-- </div> --}}
            <div class="dropdown no-arrow d-sm-block d-md-none d-lg-none d-xl-none">
                <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink"
                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                </a>
                <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in"
                    aria-labelledby="dropdownMenuLink">
                    <div class="dropdown-header">Dropdown Header:</div>
                        <a class="dropdown-item" href="{{url('/dashboard')}}">Kembali</a>
                    <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="{{url('/asset_expense/pdf')}}">Export PDF</a>
                        <a class="dropdown-item" href="{{url('/asset_expense/excel')}}">Export Excel</a>
                        <a class="dropdown-item" href="{{url('/asset_expense/create')}}">Add New</a>
                </div>
            </div>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered table-striped" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>Expense Number</th>
                            <th>Expense Date</th>
                            <th>Item Name</th>
                            <th>Location</th>
                            <th>Expense Note</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($asset_expense as $asset_expense)
                        <tr>
                            <td>{{$asset_expense->t_asset_expense_no}}</td>
                            <td>{{$asset_expense->t_asset_expense_date}}</td>
                            <td>{{$asset_expense->m_item_code}}</td>
                            <td>{{$asset_expense->m_location_desc}}</td>
                            <td>{{$asset_expense->t_asset_expense_h_note}}</td>
                            <td>
                                <div class="row text-center">
                                    <div class="col-sm-6 text-center">
                                        <button class="btn-danger btn-block btn shadow-sm" title="Hapus" onclick="deleteDetail('{{ $asset_expense->t_asset_expense_h_id }}')"><ion-icon name="trash-outline"></ion-icon></button>
                                    </div>
                                    <div class="col-sm-6 text-center">
                                        <button class="btn-warning btn-block btn shadow-sm" title="Edit" onclick="editDetail('{{ $asset_expense->t_asset_expense_h_id }}')"><ion-icon name="create-outline"></ion-icon></button>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<script type="module" src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.esm.js"></script>
<script nomodule src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.js"></script>
<script>
function deleteDetail($id){
    // Toast Sweetalert
    /* const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000,
        timerProgressBar: true,
        didOpen: (toast) => {
            toast.addEventListener('mouseenter', Swal.stopTimer)
            toast.addEventListener('mouseleave', Swal.resumeTimer)
        }
    });
    Swal.fire({
        title: 'Apakah Anda yakin untuk menghapus data ini?',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya',
        cancelButtonText: 'Batal'
    }).then((result) => {
        if (result.isConfirmed) { */
            window.location.href="{{url('asset_expense/delete')}}"+"/"+$id;
        /* }
        else {
            Toast.fire({
                icon: 'info',
                title: 'Data Tidak Jadi Dihapus'
            })
        }
    }) */
}
function editDetail($id){
    // Toast Sweetalert
    /* const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000,
        timerProgressBar: true,
        didOpen: (toast) => {
            toast.addEventListener('mouseenter', Swal.stopTimer)
            toast.addEventListener('mouseleave', Swal.resumeTimer)
        }
    }); */
    /* Swal.fire({
        title: 'Apakah Anda yakin untuk mengedit data ini?',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya',
        cancelButtonText: 'Batal'
    }).then((result) => {
        if (result.isConfirmed) { */
            window.location.href="{{url('asset_expense/edit')}}"+"/"+$id;
        /* }
        else {
            Toast.fire({
                icon: 'info',
                title: 'Data Tidak Jadi Diedit'
            })
        }
    }) */
}
$(document).ready(function() {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
});
</script>
@endsection

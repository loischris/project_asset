@extends('template.app')

@section('nav-link/report/collapse')
nav-link collapsed
@endsection
@section('nav-link/report/aria-expanded')
false
@endsection

@section('nav-link/transaction/collapse')
nav-link collapsed
@endsection
@section('nav-link/transaction/aria-expanded')
false
@endsection

@section('page-title')
Master Location - Update
@endsection

@section('nav-item/master/active')
active
@endsection

@section('nav-link/master/collapse')
nav-link
@endsection

@section('nav-link/master/aria-expanded')
true
@endsection

@section('collapse/master/show')
show
@endsection

@section('collapse-item/master/location')
active bg-light
@endsection

@section('content')
<div class="container-fluid">
    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <!-- Card Header - Dropdown -->
        <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
            <h6 class="m-0 font-weight-bold text-primary">Location</h6>
        </div>
        <form action="{{url('/location_master/update/'.$data->m_location_id)}}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="row mb-3">
                            <label class="col-sm-4 col-form-label col-form-label-sm">Group</label>
                            <div class="col-sm-8">
                                <select class="form-control form-control-sm" name="m_location_group">
                                    <option value="CV. ANUGRAH PRATAMA"
                                    @if($data->m_location_group == 'CV. ANUGRAH PRATAMA')
                                    selected
                                    @endif
                                    >CV. ANUGRAH PRATAMA</option>
                                    <option value="CV. Surabaya Karya Jaya"
                                    @if($data->m_location_group == 'CV. Surabaya Karya Jaya')
                                    selected
                                    @endif
                                    >CV. Surabaya Karya Jaya</option>
                                    <option value="CV. Surabaya Teknik"
                                    @if($data->m_location_group == 'CV. Surabaya Teknik')
                                    selected
                                    @endif
                                    >CV. Surabaya Teknik</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="row mb-3">
                            <label class="col-sm-4 col-form-label col-form-label-sm">Division</label>
                            <div class="col-sm-8">
                                <select class="form-control form-control-sm" name="m_divisi_id">
                                    <option value="DIVISI GENERAL"
                                    @if($data->m_divisi_id == 'DIVISI GENERAL')
                                    selected
                                    @endif
                                    >DIVISI GENERAL</option>
                                    <option value="CABANG RIAU"
                                    @if($data->m_divisi_id == 'CABANG RIAU')
                                    selected
                                    @endif
                                    >CABANG RIAU</option>
                                    <option value="CABANG BONDOWOSO"
                                    @if($data->m_divisi_id == 'CABANG BONDOWOSO')
                                    selected
                                    @endif
                                    >CABANG BONDOWOSO</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="row mb-3">
                            <label class="col-sm-4 col-form-label col-form-label-sm">Departement</label>
                            <div class="col-sm-8">
                                <select class="form-control form-control-sm" name="m_departemen_id">
                                    <option value="PENJUALAN"
                                    @if($data->m_departemen_id == 'PENJUALAN')
                                    selected
                                    @endif
                                    >PENJUALAN</option>
                                    <option value="PEMBELIAN"
                                    @if($data->m_departemen_id == 'PEMBELIAN')
                                    selected
                                    @endif
                                    >PEMBELIAN</option>
                                    <option value="KEUANGAN"
                                    @if($data->m_departemen_id == 'KEUANGAN')
                                    selected
                                    @endif
                                    >KEUANGAN</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="row mb-3">
                            <label class="col-sm-4 col-form-label col-form-label-sm">Location GeoTag</label>
                            <div class="col-sm-8">
                                <div class="row">
                                    <label class="col-sm-1 col-form-label col-form-label-sm">X</label>
                                    <input type="text" class="col-sm-4 form-control form-control-sm" id="loc_x" name="m_location_geotag_x" value="{{$data->m_location_geotag_x}}">
                                    <label class="col-sm-1 col-form-label col-form-label-sm">Y</label>
                                    <input type="text" class="col-sm-4 form-control form-control-sm" id="loc_y" name="m_location_geotag_y" value="{{$data->m_location_geotag_y}}">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">

                    <div class="col-md-6">
                        <div class="row mb-3">
                            <label class="col-sm-4 col-form-label col-form-label-sm">Location Photo (if any)</label>
                            <div class="col-sm-8">
                                <input type="file" class="form-control form-control-sm" value="{{ old('prscardpict') }}" id="imgInp1" accept="image/*" name="m_location_picture">
                                <img class="mt-2" style="width: 200px;" src="{{asset('img/location/'.$data->m_location_picture.'')}}" id="blah1" src="#" alt="your image" />
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="row mb-3">
                            <label class="col-sm-4 col-form-label col-form-label-sm">Location Description</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control form-control-sm" id="" name="m_location_desc" value="{{$data->m_location_desc}}">
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label class="col-sm-4 col-form-label col-form-label-sm">Location Map Picture</label>
                            <div class="col-sm-8">
                                <div id="map"
                                style="width: 300px; height: 200px"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row float-right mb-2">
                    <button type="submit" class="btn-success d-none d-sm-inline-block btn btn-md shadow-sm mr-3">Simpan</button>
                    <a href="{{url('/location_master')}}" class="btn-light d-none d-sm-inline-block btn btn-md shadow-sm">Cancel</a>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection

@section('js')
<script src="https://maps.google.com/maps/api/js?sensor=false"></script>
<script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
<script>
    $(document).ready(function () {
        lat = parseFloat(document.getElementById('loc_x').value);
        lng = parseFloat(document.getElementById('loc_y').value);
        var mapg = new google.maps.Map(document.getElementById('map'), {
            center: {
                lat: lat,
                lng: lng
            },
            zoom: 15,
            mapTypeId: 'roadmap'
        });
        var lattlong = new google.maps.LatLng(lat,lng);
        var markerg = new google.maps.Marker({
            position: lattlong,
            map: mapg,
            title: "You are here!",
        });
    });
    function generate(){
        lat = parseFloat(document.getElementById('loc_x').value);
        lng = parseFloat(document.getElementById('loc_y').value);
        var mapg = new google.maps.Map(document.getElementById('map'), {
            center: {
                lat: lat,
                lng: lng
            },
            zoom: 15,
            mapTypeId: 'roadmap'
        });
        var lattlong = new google.maps.LatLng(lat,lng);
        var markerg = new google.maps.Marker({
            position: lattlong,
            map: mapg,
            title: "You are here!",
        });
    }
    $("#loc_y").on('change', function (e) {
        generate();
    });
    $("#loc_x").on('change', function (e) {
        generate();
    });

    imgInp1.onchange = evt => {
        const [file] = imgInp1.files
        if (file) {
            blah1.src = URL.createObjectURL(file)
        }
    }
</script>
@endsection

@extends('template.app')

@section('nav-link/report/collapse')
nav-link collapsed
@endsection
@section('nav-link/report/aria-expanded')
false
@endsection

@section('nav-link/transaction/collapse')
nav-link collapsed
@endsection
@section('nav-link/transaction/aria-expanded')
false
@endsection

@section('page-title')
Master Barang - Home
@endsection

@section('nav-item/master/active')
active
@endsection

@section('nav-link/master/collapse')
nav-link
@endsection

@section('nav-link/master/aria-expanded')
true
@endsection

@section('collapse/master/show')
show
@endsection

@section('collapse-item/master/barang')
active bg-light
@endsection

@section('content')
<div class="container-fluid">
    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <!-- Card Header - Dropdown -->
        <div
        class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
        <h6 class="m-0 font-weight-bold text-primary">Barang</h6>
        <div class="dropdown no-arrow">
            <a href="{{url('/barang/create')}}" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i
                class="fas fa-plus fa-sm text-white-50"></i> Add New</a>
            </div>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>Code</th>
                            <th>Kategori 1</th>
                            <th>Kategori 2</th>
                            <th>Kategori 3</th>
                            <th>Kategori 4</th>
                            <th>Deskripsi</th>
                            <th>Unit</th>
                            <th>Status</th>
                            <th>Tipe Barang</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($barang as $bar)
                        <tr>
                            <td>{{$bar->m_item_code}}</td>
                            <td>{{$bar->m_item_cat1}}</td>
                            <td>{{$bar->m_item_cat2}}</td>
                            <td>{{$bar->m_item_cat3}}</td>
                            <td>{{$bar->m_item_cat4}}</td>
                            <td>{{$bar->m_item_cat1}}</td>
                            <td>{{$bar->m_item_unit_id}}</td>
                            <td>{{$bar->m_item_flag}}</td>
                            <td>{{$bar->m_item_type}}</td>
                            <td>
                                <div class="row">
                                    <div class="col-md-5">
                                        <button class="btn-danger d-none d-sm-inline-block btn btn-sm shadow-sm mr-3" style="font-size: 20px;" title="hapus"><ion-icon name="trash-outline" onclick="del('{{ $bar->m_item_id }}')"></ion-icon></button>
                                    </div>
                                    <div class="col-md-5">
                                        <button class="btn-warning d-none d-sm-inline-block btn btn-sm shadow-sm mr-3" style="font-size: 20px;" title="Edit"><ion-icon name="create-outline" onclick="edit('{{ $bar->m_item_id }}')"></ion-icon></button>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<script type="module" src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.esm.js"></script>
<script nomodule src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.js"></script>
<script type="text/javascript">
    function del($id){
        window.location.href="{{url('barang/delete')}}"+"/"+$id;
    }
    function edit($id){
        window.location.href="{{url('barang/edit')}}"+"/"+$id;
    }
</script>
@endsection

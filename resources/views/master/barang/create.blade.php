@extends('template.app')

@section('nav-link/report/collapse')
nav-link collapsed
@endsection
@section('nav-link/report/aria-expanded')
false
@endsection

@section('nav-link/transaction/collapse')
nav-link collapsed
@endsection
@section('nav-link/transaction/aria-expanded')
false
@endsection

@section('page-title')
Master Barang - Create
@endsection

@section('nav-item/master/active')
active
@endsection

@section('nav-link/master/collapse')
nav-link
@endsection

@section('nav-link/master/aria-expanded')
true
@endsection

@section('collapse/master/show')
show
@endsection

@section('collapse-item/master/barang')
active bg-light
@endsection

@section('content')
<div class="container-fluid">
    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <!-- Card Header - Dropdown -->
        <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
            <h6 class="m-0 font-weight-bold text-primary">Barang</h6>
        </div>
        <form action="{{url('/barang/tambah')}}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="row mb-3">
                            <label class="col-sm-4 col-form-label col-form-label-sm">Business Unit</label>
                            <div class="col-sm-8">
                                <select class="form-control form-control-sm" name="m_company_id">
                                    <option value="" selected disabled>Pilih Business</option>
                                    <option value="CV. ANUGRAH PRATAMA">CV. ANUGRAH PRATAMA</option>
                                    <option value="CV. Surabaya Karya Jaya">CV. Surabaya Karya Jaya</option>
                                    <option value="CV. Surabaya Teknik">CV. Surabaya Teknik</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="row mb-3">
                            <label class="col-sm-4 col-form-label col-form-label-sm">Divisi</label>
                            <div class="col-sm-8">
                                <select class="form-control form-control-sm" name="m_divisi_id">
                                    <option value="" selected disabled>Pilih Divisi</option>
                                    <option value="DIVISI GENERAL">DIVISI GENERAL</option>
                                    <option value="CABANG RIAU">CABANG RIAU</option>
                                    <option value="CABANG BONDOWOSO">CABANG BONDOWOSO</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="row mb-3">
                            <label class="col-sm-4 col-form-label col-form-label-sm">Location</label>
                            <div class="col-sm-8">
                                <select class="form-control form-control-sm" name="m_location_id">
                                    <option value="" selected disabled>Pilih Location</option>
                                    @foreach($location as $loc)
                                        <option value="{{$loc->m_location_id}}">{{$loc->m_location_desc}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="row mb-3">
                            <label class="col-sm-4 col-form-label col-form-label-sm">Warranty Type</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control form-control-sm" name="m_item_warranty_type" id="">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="row mb-3">
                            <label class="col-sm-4 col-form-label col-form-label-sm">Warranty Expire</label>
                            <div class="col-sm-8">
                                <input type="date" class="form-control form-control-sm" name="m_item_warranty_expire" id="">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="row mb-3">
                            <label class="col-sm-4 col-form-label col-form-label-sm">Warranty Description</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control form-control-sm" name="m_item_warranty_desc" id="">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="row mb-3">
                            <label class="col-sm-4 col-form-label col-form-label-sm">Maintenance Planning Type</label>
                            <div class="col-sm-8">
                                <select class="form-control form-control-sm" name="m_item_maintenance_plan_type">
                                    <option value="" selected disabled>Pilih Maintenance plan</option>
                                    <option value="Periodic">Periodic</option>
                                    <option value="Quantitative">Quantitative</option>
                                    <option value="Qualitative">Qualitative</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="row mb-3">
                            <label class="col-sm-4 col-form-label col-form-label-sm">Maintenance Planning</label>
                            <div class="col-sm-8">
                                <select class="form-control form-control-sm" name="m_item_maintenance_plan">
                                    <option value="" selected disabled>Pilih Maintenance plan</option>
                                    <option value="Monthly">Monthly</option>
                                    <option value="Quarter">Quarter</option>
                                    <option value="Semester">Semester</option>
                                    <option value="Yearly">Yearly</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="row mb-3">
                            <label class="col-sm-4 col-form-label col-form-label-sm">Kategori 1</label>
                            <div class="col-sm-8">
                                <select class="form-control form-control-sm" name="m_item_cat1" id="m_item_cat1">
                                    <option value="" selected disabled>Pilih Kategori</option>
                                    <option value="Fixed Asset">Fixed Asset</option>
                                    <option value="Barang">Barang</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="row mb-3">
                            <label class="col-sm-4 col-form-label col-form-label-sm">Kategori 2</label>
                            <div class="col-sm-8">
                                <select class="form-control form-control-sm" name="m_item_cat2" id="m_item_cat2">
                                    <option value="" selected disabled>Pilih Kategori</option>
                                    <option value="Accesories">Accesories</option>
                                    <option value="Abrasive">Abrasive</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="row mb-3">
                            <label class="col-sm-4 col-form-label col-form-label-sm">Kategori 3</label>
                            <div class="col-sm-8">
                                <select class="form-control form-control-sm" name="m_item_cat3" id="m_item_cat3">
                                    <option value="" selected disabled>Pilih Kategori</option>
                                    <option value="Panasonic">Panasonic</option>
                                    <option value="Fingerspot">Fingerspot</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="row mb-3">
                          <label class="col-sm-4 col-form-label col-form-label-sm">Kategori 4</label>
                          <div class="col-sm-8">
                            <select class="form-control form-control-sm" name="m_item_cat4" id="m_item_cat4">
                                <option value="" selected disabled>Pilih Kategori</option>
                                <option value="Huawei">Huawei</option>
                                <option value="Linksys">Linksys</option>
                            </select>
                          </div>
                        </div>
                    </div>

                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="row mb-3">
                            <label class="col-sm-4 col-form-label col-form-label-sm">Kode</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control form-control-sm" id="m_item_code" readonly="readonly" name="m_item_code" value="00 00 000 00 000">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="row mb-3">
                            <label class="col-sm-4 col-form-label col-form-label-sm">Deskrpsi</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control form-control-sm" id="" name="m_item_desc">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="row mb-3">
                            <label class="col-sm-4 col-form-label col-form-label-sm">Tipe Barang</label>
                            <div class="col-sm-8">
                                <input type="radio" name="m_item_type" id="m_item_type" checked value="Asset">
                                <label class="form-check-label" for="m_item_type">
                                    Asset
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="row mb-3">
                            <label class="col-sm-4 col-form-label col-form-label-sm">Satuan</label>
                            <div class="col-sm-8">
                                <select class="form-control form-control-sm" name="m_item_flag">
                                    <option value="" selected disabled>Pilih Satuan</option>
                                    <option value="Pcs">Pcs</option>
                                    <option value="Meter">Meter</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="row mb-3">
                            <label class="col-sm-4 col-form-label col-form-label-sm">Status</label>
                            <div class="col-sm-8">
                                <input type="radio" name="m_item_unit_id" id="m_item_unit_id1" checked value="Active">
                                <label class="form-check-label mr-3" for="m_item_unit_id1">
                                    Active
                                </label>
                                <input type="radio" name="m_item_unit_id" id="m_item_unit_id2" value="Inactive">
                                <label class="form-check-label mr-3" for="m_item_unit_id2">
                                    Inactive
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="row mb-3">
                            <label class="col-sm-4 col-form-label col-form-label-sm">Picture</label>
                            <div class="col-sm-8">
                                <input type="file" name="picture" class="form-control form-control-sm" value="{{ old('picture') }}" id="imgInp1" accept="image/*">
                                <img class="mt-2" style="width: 200px;" src="{{asset('img/noimage.png')}}" id="blah1" src="#" alt="your image" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row float-right mb-2">
                    <button type="submit" class="btn-success d-none d-sm-inline-block btn btn-md shadow-sm mr-3">Simpan</button>
                    <a href="{{url('barang')}}" class="btn-light d-none d-sm-inline-block btn btn-md shadow-sm">Cancel</a>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection

@section('js')
<script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
<script>
    function code(){
        var kat1 = $('#m_item_cat1').val();
        if (kat1 == "Fixed Asset") {
            var val1 = "FA";
        }else if(kat1 == "Barang"){
            var val1 = "BA";
        }else{
            var val1 = "00";
        }
        var kat2 = $('#m_item_cat2').val();
        if (kat2 == "Accesories") {
            var val2 = "01";
        }else if(kat2 == "Abrasive"){
            var val2 = "02";
        }else{
            var val2 = "00";
        }
        var kat3 = $('#m_item_cat3').val();
        if (kat3 == "Panasonic") {
            var val3 = "001";
        }else if(kat3 == "Fingerspot"){
            var val3 = "002";
        }else{
            var val3 = "000";
        }
        var kat4 = $('#m_item_cat4').val();
        if (kat4 == "Huawei") {
            var val4 = "01";
        }else if(kat4 == "Linksys"){
            var val4 = "02";
        }else{
            var val4 = "00";
        }
        var count = '{{$barang}}';
        if (count.length == 1) {
            var val5 = "00"+count;
        }else if (count.length == 2){
            var val5 = "0"+count;
        }else{
            var val5 = count;
        }
        var kode = val1+" "+val2+" "+val3+" "+val4+" "+val5;
        $('#m_item_code').val(kode);
    }
    $(document).ready(function () {
        code();
    });
    $("#m_item_cat1").on('change', function (e) {
        code();
    });
    $("#m_item_cat2").on('change', function (e) {
        code();
    });
    $("#m_item_cat3").on('change', function (e) {
        code();
    });
    $("#m_item_cat4").on('change', function (e) {
        code();
    });
    imgInp1.onchange = evt => {
        const [file] = imgInp1.files
        if (file) {
            blah1.src = URL.createObjectURL(file)
        }
    }
</script>
@endsection

@extends('template.app')

@section('nav-link/master/collapse')
nav-link collapsed
@endsection
@section('nav-link/master/aria-expanded')
false
@endsection

@section('nav-link/report/collapse')
nav-link collapsed
@endsection
@section('nav-link/report/aria-expanded')
false
@endsection

@section('page-title')
Asset Movement - Edit
@endsection

@section('nav-item/transaction/active')
active
@endsection

@section('nav-link/transaction/collapse')
nav-link
@endsection

@section('nav-link/transaction/aria-expanded')
true
@endsection

@section('collapse/transaction/show')
show
@endsection

@section('collapse-item/transaction/asset_movement')
active bg-light
@endsection

@section('content')
<div class="container-fluid">
    <div class="card shadow mb-4">
        <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
            {{-- <div class="dropdown no-arrow d-none d-lg-block d-xl-block">
                <a id="backMovementHome" href="{{url('/asset_movement')}}" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-arrow-left fa-sm text-white-50"></i> Kembali</a>
            </div> --}}
            <h6 class="m-0 font-weight-bold text-primary">Edit Asset Movement</h6>
            {{-- <div class="dropdown no-arrow d-none d-lg-block d-xl-block">
                <a id="addNewMovement" href="{{url('/asset_movement/create')}}" class="d-none d-sm-inline-block btn btn-sm btn-success shadow-sm"><i class="fas fa-plus fa-sm text-white-50"></i> Add New</a>
            </div> --}}
            <div class="dropdown no-arrow d-sm-block d-md-none d-lg-none d-xl-none">
                <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink"
                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                </a>
                <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in"
                    aria-labelledby="dropdownMenuLink">
                    <div class="dropdown-header">Dropdown Header:</div>
                        <a class="dropdown-item" href="{{url('/dashboard')}}">Kembali</a>
                    <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="{{url('/asset_movement/create')}}">Add New</a>
                </div>
            </div>
        </div>
        <div class="card-body">
            <form method="POST" enctype="multipart/form-data" id="form_asset_movement_update" action="javascript:void(0)">
                @csrf
                <div class="row">
                    <div class="col-md-6">
                        <div class="row mb-3">
                            <label class="col-sm-4 col-form-label col-form-label-sm">Movement Number</label>
                            <div class="col-sm-8">
                                <input required="required" type="text" class="form-control form-control-sm" value="{{$asset->t_asset_move_no}}" name="t_asset_move_no" id="t_asset_move_no">
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label class="col-sm-4 col-form-label col-form-label-sm">Movement Date</label>
                            <div class="col-sm-8">
                                <input required="required" type="date" class="form-control form-control-sm" value="{{$asset->t_asset_move_date}}" name="t_asset_move_date" id="t_asset_move_date">
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label class="col-sm-4 col-form-label col-form-label-sm">Movement from Location</label>
                            <div class="col-sm-8">
                                <select required="required" class="form-control form-control-sm" name="t_asset_mov_from_location_id" id="t_asset_mov_from_location_id">
                                    <option selected disabled="disabled">Pilih Lokasi</option>
                                    @foreach($location as $loca)
                                    <option value="{{$loca->m_location_id}}"
                                        @if($asset->t_asset_mov_from_location_id == $loca->m_location_id)
                                        selected
                                        @endif
                                        >{{$loca->m_location_desc}}
                                    </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label class="col-sm-4 col-form-label col-form-label-sm">Movement to Location</label>
                            <div class="col-sm-8">
                                <select required="required" class="form-control form-control-sm" name="t_asset_mov_to_location_id" id="t_asset_mov_to_location_id">
                                    <option selected disabled="disabled">Pilih Lokasi</option>
                                    @foreach($location as $loca)
                                    <option value="{{$loca->m_location_id}}"
                                        @if($asset->t_asset_mov_to_location_id == $loca->m_location_id)
                                        selected
                                        @endif
                                        >{{$loca->m_location_desc}}
                                    </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="row mb-3">
                            <label class="col-sm-4 col-form-label col-form-label-sm">Movement Respon</label>
                            <div class="col-sm-8">
                                <select required="required" class="form-control form-control-sm" name="t_asset_mov_respon" id="t_asset_mov_respon">
                                    <option selected="selected" disabled="disabled">Pilih Respon</option>
                                    <option {{ $asset->t_asset_mov_respon == '1' ? "selected" : "" }} value="1">OK</option>
                                    <option {{ $asset->t_asset_mov_respon == '2' ? "selected" : "" }} value="2">NOT OK</option>
                                </select>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label class="col-sm-4 col-form-label col-form-label-sm">Movement Method</label>
                            <div class="col-sm-8">
                                <select required="required" class="form-control form-control-sm" name="t_asset_mov_method" id="t_asset_mov_method">
                                    <option selected="selected" disabled="disabled">Pilih Method</option>
                                    <option {{ $asset->t_asset_mov_method == '1' ? "selected" : "" }} value="1">Method A</option>
                                    <option {{ $asset->t_asset_mov_method == '2' ? "selected" : "" }} value="2">Method B</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row text-right">
                    <div class="col-lg">
                        <button type="submit" id="submitButtonUp" class="btn btn-success shadow-sm">Edit Data</button>
                        <button type="reset" id="resetButtonUp" class="btn btn-secondary shadow-sm">Batal</button>
                    </div>
                </div>
            </form>

            <div class="modal fade" id="assetMovementModal" tabindex="-1" role="dialog" aria-labelledby="assetMovementLabel" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="assetMovementLabel">New Detail Asset Movement</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="resetter()">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <form method="POST" enctype="multipart/form-data" id="form_detail_asset_movement" action="javascript:void(0)" >
                            <div class="modal-body">
                                <div class="row mb-3">
                                    <label class="col-sm-4 col-form-label col-form-label-sm">Asset Movement</label>
                                    <div class="col-sm-8">
                                        <input type="hidden" name="t_asset_move_d_id" id="t_asset_move_d_id">
                                        <input type="hidden" name="t_asset_move_h_id" id="t_asset_move_h_id" value="{{$asset->t_asset_move_h_id}}">
                                        <input type="text" class="form-control form-control-sm" id="t_asset_move_no" disabled="disabled" value="{{$asset->t_asset_move_no}}">
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label class="col-sm-4 col-form-label col-form-label-sm">Item</label>
                                    <div class="col-sm-8">
                                        <select required="required" class="form-control form-control-sm" name="m_item_id" id="m_item_id">
                                            <option selected="selected" disabled="disabled">Pilih Item Barang-barang</option>
                                            @foreach($barang as $bar)
                                            <option value="{{$bar->m_item_id}}">{{$bar->m_item_code}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary shadow-sm" data-dismiss="modal">Tutup</button>
                                <button type="submit" class="btn btn-primary shadow-sm" id="buttonData">Simpan</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <div class="dropdown-divider"></div>

            <div class="row my-4">
                <div class="col-md-12 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary">Detail Asset Movement</h6>
                    <div class="dropdown no-arrow">
                        <button type="button" id="addButtonModal" data-toggle="modal" data-target="#assetMovementModal" class="btn btn-sm btn-primary shadow-sm"><i class="fas fa-plus fa-sm text-white-50"></i> Add New</button>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="table-responsive">
                        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>Kode</th>
                                    <th>Item</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($detail as $det)
                                    <tr>
                                        <td>{{$det->t_asset_move_no}}</td>
                                        <td>{{$det->m_item_code}}</td>
                                        <td>
                                            <div class="row text-center">
                                                <div class="col-sm-6">
                                                    <button id="deleteButtonModal" class="btn-danger btn-block btn btn-md shadow-sm" title="hapus" onclick="deleteDetail('{{ $det->t_asset_move_d_id }}')"><ion-icon name="trash-outline"></ion-icon></button>
                                                </div>
                                                <div class="col-sm-6">
                                                    <button id="editButtonModal" class="btn-warning btn-block btn btn-md shadow-sm" data-toggle="modal" data-target="#assetMovementModal" title="Edit" onclick="editDetail('{{ $det->t_asset_move_d_id }}')"><ion-icon name="create-outline"></ion-icon></button>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <br>
                    <div class="row text-right">
                        <div class="col-lg">
                            <button id="saveButtonDown" class="btn btn-success shadow-sm">Simpan Semua</button>
                            <button id="cancelButtonDown" type="reset" class="btn btn-secondary shadow-sm">Batal Data</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<script src="https://cdn.ckeditor.com/4.16.2/standard/ckeditor.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfobject/2.2.7/pdfobject.min.js" integrity="sha512-g16L6hyoieygYYZrtuzScNFXrrbJo/lj9+1AYsw+0CYYYZ6lx5J3x9Yyzsm+D37/7jMIGh0fDqdvyYkNWbuYuA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script type="module" src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.esm.js"></script>
<script nomodule src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.js"></script>
<script>
function deleteDetail(id){
    // Toast Sweetalert
    /* const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000,
        timerProgressBar: true,
        didOpen: (toast) => {
            toast.addEventListener('mouseenter', Swal.stopTimer)
            toast.addEventListener('mouseleave', Swal.resumeTimer)
        }
    }); */
    /* Swal.fire({
        title: 'Apakah Anda yakin ingin menghapus data detail ini?',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya',
        cancelButtonText: 'Tidak'
    }).then((result) => {
        if (result.isConfirmed) { */
            $.ajax({
                type: 'GET',
                url: "{{url('asset_movement/update/delete')}}"+"/"+id,
                success: function(data) {
                    /* Toast.fire({
                        icon: 'success',
                        title: 'Data Detail telah dihapus.'
                    }) */
                    /* setInterval("location.reload();", 3000); */
                    location.reload();
                },
                error: function() {
                    /* Toast.fire({
                        icon: 'warning',
                        title: 'Maaf, Error.'
                    }) */
                }
            });
        /* }
        else {
            Toast.fire({
                icon: 'info',
                title: 'Silahkan check kembali.'
            })
        }
    }) */
}
function editDetail(id){
    $.ajax({
        type:'GET',
        url: "{{url('/asset_movement/getdetail')}}"+"/"+id,
        success: (data) => {
            $('#t_asset_move_d_id').val(id);
            $('#t_asset_move_h_id').val(data.t_asset_move_h_id);
            $('#m_item_id').val(data.m_item_id);
        },
        error: function(data){},
        complete: function() {}
    });
};
$(document).ready(function() {
    // Toast Sweetalert
    /* const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000,
        timerProgressBar: true,
        didOpen: (toast) => {
            toast.addEventListener('mouseenter', Swal.stopTimer)
            toast.addEventListener('mouseleave', Swal.resumeTimer)
        }
    }); */
    // KeyUp validator
    /* $("#t_asset_move_no").keyup(function () {
        if ($("#t_asset_move_no").val()) {
            $("#t_asset_move_no").removeClass("is-invalid").addClass("is-valid").addClass("is-valid");
        }
        else {
            $("#t_asset_move_no").removeClass("is-valid").addClass("is-invalid").addClass("is-invalid");
        }
    });
    $("#t_asset_move_date").change(function () {
        if ($("#t_asset_move_date").val()) {
            $("#t_asset_move_date").removeClass("is-invalid").addClass("is-valid").addClass("is-valid");
        }
        else {
            $("#t_asset_move_date").removeClass("is-valid").addClass("is-invalid").addClass("is-invalid");
        }
    });
    $("#t_asset_mov_from_location_id").change(function () {
        if ($("#t_asset_mov_from_location_id").val()) {
            $("#t_asset_mov_from_location_id").removeClass("is-invalid").addClass("is-valid");
        }
        else {
            $("#t_asset_mov_from_location_id").removeClass("is-valid").addClass("is-invalid");
        }
    });
    $("#t_asset_mov_to_location_id").change(function () {
        if ($("#t_asset_mov_to_location_id").val()) {
            $("#t_asset_mov_to_location_id").removeClass("is-invalid").addClass("is-valid");
        }
        else {
            $("#t_asset_mov_to_location_id").removeClass("is-valid").addClass("is-invalid");
        }
    });
    $("#t_asset_mov_respon").change(function () {
        if ($("#t_asset_mov_respon").val()) {
            $("#t_asset_mov_respon").removeClass("is-invalid").addClass("is-valid");
        }
        else {
            $("#t_asset_mov_respon").removeClass("is-valid").addClass("is-invalid");
        }
    });
    $("#t_asset_mov_method").change(function () {
        if ($("#t_asset_mov_method").val()) {
            $("#t_asset_mov_method").removeClass("is-invalid").addClass("is-valid");
        }
        else {
            $("#t_asset_mov_method").removeClass("is-valid").addClass("is-invalid");
        }
    });
    $("#m_item_id").change(function () {
        if ($("#m_item_id").val()) {
            $("#m_item_id").removeClass("is-invalid").addClass("is-valid");
        }
        else {
            $("#m_item_id").removeClass("is-valid").addClass("is-invalid");
        }
    }); */
    // Enabled Input
    $('#t_asset_move_no').removeClass('disabled').addClass('enabled').attr('enabled','enabled').removeAttr('disabled','disabled');
    $('#t_asset_move_date_id').removeClass('disabled').addClass('enabled').attr('enabled','enabled').removeAttr('disabled','disabled');
    $('#t_asset_mov_from_location_id').removeClass('disabled').addClass('enabled').attr('enabled','enabled').removeAttr('disabled','disabled');
    $('#t_asset_mov_to_location_id').removeClass('disabled').addClass('enabled').attr('enabled','enabled').removeAttr('disabled','disabled');
    $('#t_asset_mov_respon').removeClass('disabled').addClass('enabled').attr('enabled','enabled').removeAttr('disabled','disabled');
    $('#t_asset_mov_method').removeClass('disabled').addClass('enabled').attr('enabled','enabled').removeAttr('disabled','disabled');
    // Ajax Setup
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    // Cancel Data
    $('#resetButtonUp').click(function(){
        /* Swal.fire({
            title: 'Apakah Anda yakin ingin mengedit data ini lagi?',
            icon: 'warning',
            showCancelButton: true,
            cancelButtonColor: '#d33',
            confirmButtonColor: '#3085d6',
            confirmButtonText: 'Ya',
            cancelButtonText: 'Tidak'
        }).then((result) => {
            if (result.isConfirmed) {
                Toast.fire({
                    icon: 'info',
                    title: 'Silahkan mengedit data dengan benar.'
                }) */
                $('#t_asset_move_no').removeClass('disabled').addClass('enabled').attr('enabled','enabled').removeAttr('disabled','disabled');
                $('#t_asset_move_date_id').removeClass('disabled').addClass('enabled').attr('enabled','enabled').removeAttr('disabled','disabled');
                $('#t_asset_mov_from_location_id').removeClass('disabled').addClass('enabled').attr('enabled','enabled').removeAttr('disabled','disabled');
                $('#t_asset_mov_to_location_id').removeClass('disabled').addClass('enabled').attr('enabled','enabled').removeAttr('disabled','disabled');
                $('#t_asset_mov_respon').removeClass('disabled').addClass('enabled').attr('enabled','enabled').removeAttr('disabled','disabled');
                $('#t_asset_mov_method').removeClass('disabled').addClass('enabled').attr('enabled','enabled').removeAttr('disabled','disabled');
                $("#assetMovementModal .close").click();
            /* }
            else {
                Toast.fire({
                    icon: 'info',
                    title: 'Data Tidak Jadi di Edit'
                })
            }
        }) */
    });
    // Ajax Setup - Submit Data
    $('#submitButtonUp').click(function(){
        $('#form_asset_movement_update').submit(function(e) {
            e.preventDefault();
            /* Swal.fire({
                title: 'Apakah Anda yakin untuk mengedit data ini?',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Kirim',
                cancelButtonText: 'Batal'
            }).then((result) => {
                if (result.isConfirmed) { */
                    var me1 = $(this);
                    if ( me1.data('requestRunning') ) {
                        return;
                    }
                    me1.data('requestRunning', true);
                    var formData = new FormData(this);
                    $.ajax({
                        type:'POST',
                        url: "{{url('/asset_movement/update/'.$asset->t_asset_move_h_id)}}",
                        data: formData,
                        cache:false,
                        contentType: false,
                        processData: false,
                        success: (data) => {
                            $('#t_asset_move_h_id').val(data.t_asset_move_h_id);
                            $('#t_asset_move_no').val(data.t_asset_move_no).removeClass('enabled').addClass('disabled').attr('disabled','disabled').removeAttr('enabled','enabled');
                            $('#t_asset_move_date_id').removeClass('enabled').addClass('disabled').attr('disabled','disabled').removeAttr('enabled','enabled');
                            $('#t_asset_mov_from_location_id').removeClass('enabled').addClass('disabled').attr('disabled','disabled').removeAttr('enabled','enabled');
                            $('#t_asset_mov_to_location_id').removeClass('enabled').addClass('disabled').attr('disabled','disabled').removeAttr('enabled','enabled');
                            $('#t_asset_mov_respon').removeClass('enabled').addClass('disabled').attr('disabled','disabled').removeAttr('enabled','enabled');
                            $('#t_asset_mov_method').removeClass('enabled').addClass('disabled').attr('disabled','disabled').removeAttr('enabled','enabled');
                            $('#m_item_id').removeClass('enabled').addClass('disabled').attr('disabled','disabled').removeAttr('enabled','enabled');
                            $('#backMovementHome').removeClass('enabled').addClass('disabled').attr('disabled','disabled').removeAttr('enabled','enabled');
                            $('#addNewMovement').removeClass('enabled').addClass('disabled').attr('disabled','disabled').removeAttr('enabled','enabled');
                            $('#resetButtonUp').removeClass('enabled').addClass('disabled').attr('disabled','disabled').removeAttr('enabled','enabled');
                            $('#submitButtonUp').removeClass('enabled').addClass('disabled').attr('disabled','disabled').removeAttr('enabled','enabled');
                            $('#editButtonModal').removeClass('enabled').addClass('disabled').attr('disabled','disabled').removeAttr('enabled','enabled');
                            $('#addButtonModal').removeClass('enabled').addClass('disabled').attr('disabled','disabled').removeAttr('enabled','enabled');
                            $('#saveButtonDown').removeClass('enabled').addClass('disabled').attr('disabled','disabled').removeAttr('enabled','enabled');
                            $('#cancelButtonDown').removeClass('enabled').addClass('disabled').attr('disabled','disabled').removeAttr('enabled','enabled');
                            $('#deleteButtonModal').removeClass('enabled').addClass('disabled').attr('disabled','disabled').removeAttr('enabled','enabled');
                            /* Toast.fire({
                                icon: 'success',
                                title: 'Data Berhasil di Edit'
                            })
                            setInterval("location.reload();", 3000); */
                            location.reload();
                            $("#assetMovementModal .close").click();
                        },
                        error: function(data){
                            /* Toast.fire({
                                icon: 'error',
                                title: 'Data Tidak Berhasil di Edit'
                            }) */
                        },
                        complete: function() {
                            me1.data('requestRunning', false);
                        }
                    });
                /* }
                else {
                    Toast.fire({
                        icon: 'warning',
                        title: 'Data Tidak Jadi di Edit'
                    })
                }
            }) */
        });
    });
    // Edit Detail
    var no = 1;
    $('#editButtonModal').click(function(){
        $('#assetMovementLabel').html('Edit Data Detail');
        $('#form_detail_asset_movement').attr('Edit','Editable').removeAttr('Add','Addable');
        var t_asset_move_h_id = $('#t_asset_move_h_id').val();
        var m_item_id = $('#m_item_id').val();
        $('#form_detail_asset_movement').submit(function(e) {
            e.preventDefault();
            /* Swal.fire({
                title: 'Apakah Anda yakin untuk mengedit data detail ini?',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Kirim',
                cancelButtonText: 'Batal'
            }).then((result) => {
                if (result.isConfirmed) { */
                    var me2 = $(this);
                    if (me2.data('requestRunning')) {
                        return;
                    }
                    me2.data('requestRunning', true);
                    var formDataDetail = new FormData(this);
                    var id = $('#t_asset_move_d_id').val();
                    $.ajax({
                        type: 'POST',
                        url: "{{ url('/asset_movement/updatedetail')}}"+"/"+id,
                        data: formDataDetail,
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: (data) => {
                            $('#t_asset_move_h_id').val(data.t_asset_move_h_id);
                            $('#t_asset_move_no').val(data.t_asset_move_no).removeClass('enabled').addClass('disabled').attr('disabled','disabled').removeAttr('enabled','enabled');
                            $('#t_asset_move_date_id').removeClass('enabled').addClass('disabled').attr('disabled','disabled').removeAttr('enabled','enabled');
                            $('#t_asset_mov_from_location_id').removeClass('enabled').addClass('disabled').attr('disabled','disabled').removeAttr('enabled','enabled');
                            $('#t_asset_mov_to_location_id').removeClass('enabled').addClass('disabled').attr('disabled','disabled').removeAttr('enabled','enabled');
                            $('#t_asset_mov_respon').removeClass('enabled').addClass('disabled').attr('disabled','disabled').removeAttr('enabled','enabled');
                            $('#t_asset_mov_method').removeClass('enabled').addClass('disabled').attr('disabled','disabled').removeAttr('enabled','enabled');
                            $('#m_item_id').removeClass('enabled').addClass('disabled').attr('disabled','disabled').removeAttr('enabled','enabled');
                            $('#backMovementHome').removeClass('enabled').addClass('disabled').attr('disabled','disabled').removeAttr('enabled','enabled');
                            $('#addNewMovement').removeClass('enabled').addClass('disabled').attr('disabled','disabled').removeAttr('enabled','enabled');
                            $('#resetButtonUp').removeClass('enabled').addClass('disabled').attr('disabled','disabled').removeAttr('enabled','enabled');
                            $('#submitButtonUp').removeClass('enabled').addClass('disabled').attr('disabled','disabled').removeAttr('enabled','enabled');
                            $('#editButtonModal').removeClass('enabled').addClass('disabled').attr('disabled','disabled').removeAttr('enabled','enabled');
                            $('#addButtonModal').removeClass('enabled').addClass('disabled').attr('disabled','disabled').removeAttr('enabled','enabled');
                            $('#saveButtonDown').removeClass('enabled').addClass('disabled').attr('disabled','disabled').removeAttr('enabled','enabled');
                            $('#cancelButtonDown').removeClass('enabled').addClass('disabled').attr('disabled','disabled').removeAttr('enabled','enabled');
                            $('#deleteButtonModal').removeClass('enabled').addClass('disabled').attr('disabled','disabled').removeAttr('enabled','enabled');
                            /* Toast.fire({
                                icon: 'success',
                                title: 'Data Berhasil di Edit'
                            })
                            setInterval("location.reload();", 3000); */
                            location.reload();
                            $("#assetMovementModal .close").click();
                        },
                        error: function(data){
                            /* Toast.fire({
                                icon: 'error',
                                title: 'Data Detail Tidak Berhasil di Edit'
                            }) */
                            //console.log(data);
                        },
                        complete: function() {
                            me2.data('requestRunning', false);
                        }
                    });
                /* }
                else {
                    Toast.fire({
                        icon: 'error',
                        title: 'Data Detail Tidak Berhasil di Edit'
                    })
                }
            }) */
        });
    });
    // Add Button
    $('#addButtonModal').click(function(){
        $('#assetMovementLabel').html('Tambah Data Detail');
        $('#form_detail_asset_movement').removeAttr('Edit','Editable').attr('Add','Addable');
        $('#m_item_id').change();
        $('#form_detail_asset_movement').submit(function(e) {
            var number = $('#t_asset_move_no').val();
            var item = $('#m_item_id').val();
            var tableBody = $('table tbody');
            var markup = "";
            markup += `<tr>`;
            markup += `<td>`;
            markup += number;
            markup += `</td>`;
            markup += `<td>`;
            markup += item;
            markup += `</td>`;
            markup += `</tr>`;
            tableBody.append(markup);
            e.preventDefault();
            /* Swal.fire({
                title: 'Apakah Anda yakin untuk menambah data detail ini?',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Kirim',
                cancelButtonText: 'Batal'
            }).then((result) => {
                if (result.isConfirmed) { */
                    var me2 = $(this);
                    if (me2.data('requestRunning')) {
                        return;
                    }
                    me2.data('requestRunning', true);
                    var formDataDetail = new FormData(this);
                    $.ajax({
                        type:'POST',
                        url: "{{ url('/asset_movement/storedetail')}}",
                        data: formDataDetail,
                        cache:false,
                        contentType: false,
                        processData: false,
                        success: (data) => {
                            /* var number = $('#t_asset_move_no').val();
                            var item = $('#m_item_id').val();
                            var tableBody = $('table tbody');
                            var markup = "";
                            markup += `<tr>`;
                            markup += `<td>`;
                            markup += number;
                            markup += `</td>`;
                            markup += `<td>`;
                            markup += item;
                            markup += `</td>`;
                            markup += `</tr>`;
                            tableBody.append(markup); */
                            $('#t_asset_move_h_id').val(data.t_asset_move_h_id);
                            $('#t_asset_move_no').val(data.t_asset_move_no).removeClass('enabled').addClass('disabled').attr('disabled','disabled').removeAttr('enabled','enabled');
                            $('#t_asset_move_date_id').removeClass('enabled').addClass('disabled').attr('disabled','disabled').removeAttr('enabled','enabled');
                            $('#t_asset_mov_from_location_id').removeClass('enabled').addClass('disabled').attr('disabled','disabled').removeAttr('enabled','enabled');
                            $('#t_asset_mov_to_location_id').removeClass('enabled').addClass('disabled').attr('disabled','disabled').removeAttr('enabled','enabled');
                            $('#t_asset_mov_respon').removeClass('enabled').addClass('disabled').attr('disabled','disabled').removeAttr('enabled','enabled');
                            $('#t_asset_mov_method').removeClass('enabled').addClass('disabled').attr('disabled','disabled').removeAttr('enabled','enabled');
                            $('#m_item_id').removeClass('enabled').addClass('disabled').attr('disabled','disabled').removeAttr('enabled','enabled');
                            $('#backMovementHome').removeClass('enabled').addClass('disabled').attr('disabled','disabled').removeAttr('enabled','enabled');
                            $('#addNewMovement').removeClass('enabled').addClass('disabled').attr('disabled','disabled').removeAttr('enabled','enabled');
                            $('#resetButtonUp').removeClass('enabled').addClass('disabled').attr('disabled','disabled').removeAttr('enabled','enabled');
                            $('#submitButtonUp').removeClass('enabled').addClass('disabled').attr('disabled','disabled').removeAttr('enabled','enabled');
                            $('#editButtonModal').removeClass('enabled').addClass('disabled').attr('disabled','disabled').removeAttr('enabled','enabled');
                            $('#addButtonModal').removeClass('enabled').addClass('disabled').attr('disabled','disabled').removeAttr('enabled','enabled');
                            $('#saveButtonDown').removeClass('enabled').addClass('disabled').attr('disabled','disabled').removeAttr('enabled','enabled');
                            $('#cancelButtonDown').removeClass('enabled').addClass('disabled').attr('disabled','disabled').removeAttr('enabled','enabled');
                            $('#deleteButtonModal').removeClass('enabled').addClass('disabled').attr('disabled','disabled').removeAttr('enabled','enabled');
                            /* Toast.fire({
                                icon: 'success',
                                title: 'Data Berhasil di Tambah'
                            })
                            setInterval("location.reload();", 3000); */
                            location.reload();
                            $("#assetMovementModal .close").click();
                        },
                        error: function(data){
                            /* Toast.fire({
                                icon: 'error',
                                title: 'Data Detail Tidak Berhasil di Tambah'
                            }) */
                        },
                        complete: function() {
                            me2.data('requestRunning', false);
                        }
                    });
                /* }
                else {
                    Toast.fire({
                        icon: 'warning',
                        title: 'Data Detail Tidak Jadi di Tambah'
                    })
                }
            }) */
        });
    });
    // Save Button Down
    $('#saveButtonDown').click(function(){
        /* Swal.fire({
            title: 'Apakah Anda yakin untuk menyimpan semua data-data detail ini?',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ya',
            cancelButtonText: 'Tidak'
        }).then((result) => {
            if (result.isConfirmed) { */
                window.location.href="/asset_movement/";
                $("#assetMovementModal .close").click();
            /* }
            else {
                Toast.fire({
                    icon: 'warning',
                    title: 'Harap edit lebih teliti.'
                })
            }
        }) */
    });
    // Cancel Button Down
    $('#cancelButtonDown').click(function(){
        /* Swal.fire({
            title: 'Apakah Anda yakin untuk membatalkan semua data-data detail ini?',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ya',
            cancelButtonText: 'Tidak'
        }).then((result) => {
            if (result.isConfirmed) { */
                window.location.href="/asset_movement/";
                $("#assetMovementModal .close").click();
            /* }
            else {
                Toast.fire({
                    icon: 'warning',
                    title: 'Harap edit lebih teliti.'
                })
            }
        }) */
    });
});
</script>
@endsection

@section('page-title')
Report Asset &amp; Location
@endsection

<html>
<head>
    <title>report_asset_location</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
    <style type="text/css">
        img{
            max-width: 100%;
        }
        body{
            font-size: 11px;
        }
        @page{
            size : A4 portrait;
        }
        table {
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width: 100%;
            font-size: 12px;
        }

        th {
            border: 1px solid #808080;
            background-color: #dddddd;
            text-align: center;
            padding: 8px;
        }

        td {
            border: 1px solid #808080;
            //text-align: right;
            padding: 8px 8px 0px 8px;
        }

        tr:nth-child(even) {
            background-color: #F5F5F5;
        }
    </style>
@foreach($barang as $bar)
<br>
    <p align="center" style="font-size: 18px;font-family: arial;">Barang code {{$bar->m_item_code}}</p>
    <table>
        <thead>
            <tr style="font-weight: bold;background: #777;color: #eee">
                <td>Description</td>
                <td>Bussines</td>
                <td>Divisi</td>
                <td>Location</td>
                <td>Item Unit</td>
                <td>Kat 1</td>
                <td>Kat 2</td>
                <td>Kat 3</td>
                <td>Kat 4</td>
                <td>Item Type</td>
                <td>Item Flag</td>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>{{$bar->m_item_desc}}</td>
                <td>{{$bar->m_company_id}}</td>
                <td>{{$bar->m_divisi_id}}</td>
                <td>{{$bar->m_location_desc}}</td>
                <td>{{$bar->m_item_unit_id}}</td>
                <td>{{$bar->m_item_cat1}}</td>
                <td>{{$bar->m_item_cat2}}</td>
                <td>{{$bar->m_item_cat3}}</td>
                <td>{{$bar->m_item_cat4}}</td>
                <td>{{$bar->m_item_type}}</td>
                <td>{{$bar->m_item_flag}}</td>
            </tr>
        </tbody>
    </table>
    <?php
        $no = 1;
    ?>
    @foreach($asset as $set)
    @if($set->m_item_id == $bar->m_item_id)
    <br>
    <p style="margin-left: 20px;font-size: 12px;font-family: arial;">#{{$no++}} Asset Confirm NO {{$set->t_asset_no}}</p>
    <table>
        <thead>
            <tr style="font-weight: bold;background: #777;color: #eee">
                <td style="font-size: 12px;">Asset No</td>
                <td style="font-size: 12px;">Bussines</td>
                <td style="font-size: 12px;">Divisi</td>
                <td style="font-size: 12px;">Date</td>
                <td style="font-size: 12px;">Asset Qty</td>
                <td style="font-size: 12px;">Asset Price</td>
                <td style="font-size: 12px;">Asset dep Month</td>
                <td style="font-size: 12px;">Asset dep Value</td>
                <td style="font-size: 12px;">Asset Status</td>
                <td style="font-size: 12px;">Asset Note</td>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td style="font-size: 12px;">{{$set->t_asset_no}}</td>
                <td style="font-size: 12px;">{{$set->m_company_id}}</td>
                <td style="font-size: 12px;">{{$set->m_divisi_id}}</td>
                <td style="font-size: 12px;">{{$set->t_asset_date}}</td>
                <td style="font-size: 12px;">{{$set->t_asset_qty}}</td>
                <td style="font-size: 12px;">Rp {{$set->t_asset_price}}</td>
                <td style="font-size: 12px;">{{$set->t_asset_dep_month}}</td>
                <td style="font-size: 12px;">Rp {{$set->t_asset_dep_value}}</td>
                <td style="font-size: 12px;">{{$set->t_asset_h_status}}</td>
                <td style="font-size: 12px;">{{$set->t_asset_h_note}}</td>
            </tr>
        </tbody>
    </table>
    <p style="margin-top: 5px;margin-bottom: 5px;margin-left: 20px;font-size: 14px;font-family: arial;">Detail</p>
    <table>
        <thead>
            <tr style="font-weight: bold;background: #777;color: #eee">
                <td>No.</td>
                <td>Period</td>
                <td>Value</td>
                <td>Depreciation</td>
            </tr>
        </thead>
        <tbody>
            <?php
            $month = ["","January", "February", "March", "April", "May", "June",
            "July", "August", "September", "October", "November", "December"
            ];
            ?>
            @foreach($detail as $det)
            @if($det->t_asset_h_id == $set->t_asset_h_id)
            <tr>
                <td>{{$det->t_asset_d_seq}}</td>
                <td>{{$month[$det->t_asset_d_month]}} {{$det->t_asset_d_year}}</td>
                <td>{{$det->t_asset_d_value}}</td>
                <td>{{$det->t_asset_d_accum}}</td>
            </tr>
            @endif
            @endforeach
        </tbody>
    </table>
    @endif
    @endforeach
@endforeach
</body>
</html>

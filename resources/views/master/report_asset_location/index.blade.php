@extends('template.app')

@section('nav-link/master/collapse')
nav-link collapsed
@endsection
@section('nav-link/master/aria-expanded')
false
@endsection

@section('nav-link/transaction/collapse')
nav-link collapsed
@endsection
@section('nav-link/transaction/aria-expanded')
false
@endsection

@section('page-title')
Report Asset &amp; Location - Index
@endsection

@section('nav-item/report/active')
active
@endsection

@section('nav-link/report/collapse')
nav-link
@endsection

@section('nav-link/report/aria-expanded')
true
@endsection

@section('collapse/report/show')
show
@endsection

@section('collapse-item/report/report')
active bg-light
@endsection

@section('content')
<div class="container-fluid">
    <!-- DataTales Example -->
    <div class="card shadow mb-4 w-50">
        <!-- Card Header - Dropdown -->
        <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
            <h6 class="m-0 font-weight-bold text-primary">Report Asset & Location</h6>
        </div>
        <div class="card-body">
            <div class="row mb-3">
                <label class="col-sm-2 col-form-label col-form-label-sm">Asset</label>
                <div class="col-sm-10">
                    <select class="form-control form-control-sm" id="asset">
                        <option value="all">All Asset</option>
                        @foreach($asset as $set)
                        <option value="{{$set->m_item_id}}">{{$set->m_item_code}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="row mb-3">
                <label class="col-sm-2 col-form-label col-form-label-sm">Location</label>
                <div class="col-sm-10">
                    <select class="form-control form-control-sm" id="location">
                        <option value="all">All location</option>
                        @foreach($location as $loc)
                        <option value="{{$loc->m_location_id}}">{{$loc->m_location_desc}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="row mb-3">
                <label class="col-sm-2 col-form-label col-form-label-sm">Type</label>
                <div class="col-sm-10">
                    <select class="form-control form-control-sm" id="type">
                        <option value="no">Without Maps</option>
                        <option value="yes">With Maps</option>
                    </select>
                </div>
            </div>
            <div class="row float-right">
                <button class="btn-success d-none d-sm-inline-block btn btn-md shadow-sm mr-3" onclick="exportPDF()">
                    Export</button>
                    <button class="btn-secondary d-none d-sm-inline-block btn btn-md shadow-sm" onclick="viewPDF()">
                        View</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
<script>
    function exportPDF(){
        var asset = $('#asset').val();
        var location = $('#location').val();
        var type = $('#type').val();
        if (asset != "" && location != "" && type != "yes") {
            window.open('{{url("report_asset_location/export/")}}/'+asset+"/"+location+"/"+type, '_blank');
        }
    }
    function viewPDF(){
        var asset = $('#asset').val();
        var location = $('#location').val();
        var type = $('#type').val();
        if (asset != "" && location != "") {
            window.open('{{url("report_asset_location/view/")}}/'+asset+"/"+location+"/"+type, '_blank');
        }

    }
    // $("#asset").click(function(){
    //     var datab = $("#asset").val();
    //     $("#asset").on('change', function (e) {
    //         e.preventDefault();
    //         if (datab != $("#asset").val()) {
    //             var me1 = $(this);
    //             if (me1.data('requestRunning')) {
    //                 return;
    //             }
    //             me1.data('requestRunning', true);
    //             var id = $('#asset').val();
    //             if (id != "all") {
    //                 $.ajax({
    //                     url: "{{ url('report_asset_location/getloc') }}"+"/"+id,
    //                     type: "GET",
    //                     success: function(data){
    //                         $('#location').val(data.m_location_id).trigger('change');
    //                     },
    //                     complete: function() {
    //                         me1.data('requestRunning', false);
    //                     }
    //                 });
    //             }else{
    //                 $('#location').val('all').trigger('change');
    //             }
    //         }
    //     });
    // });
    $("#location").on('change', function (e) {
        e.preventDefault();
        var me2 = $(this);
        if (me2.data('requestRunning')) {
            return;
        }
        me2.data('requestRunning', true);
        var id = $('#location').val();
        var asset = $('#asset').val();
        $.ajax({
            url: "{{ url('report_asset_location/getset') }}"+"/"+id,
            type: "GET",
            success: function(data){
                $('#asset').children('option:not(:first)').remove().end();
                // console.log(data);
                $.each(data,function(index,object){
                    $('#asset').append('<option value="'+object.m_item_id+'"> '+object.m_item_code+'</option>');
                    if (object.m_item_id == asset) {
                        $('#asset').val(asset).trigger('change');
                    }
                });
            },
            complete: function() {
                me2.data('requestRunning', false);
            }
        });
    });
</script>
@endsection

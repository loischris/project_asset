@section('page-title')
Report Asset &amp; Location
@endsection

<html>
<head>
    <title>Maps Asset Location</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
    <center>
        <div id="map" style="width: 100%; height: 100%"></div>
    </center>
    <script src="https://maps.google.com/maps/api/js?sensor=false"></script>
    <script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
    <script>
        $(document).ready(function () {
            latt = parseFloat(-7.3313141);
            long = parseFloat(112.7800223);
            var lattlong = new google.maps.LatLng(latt,long);
            var OPTions = {
                center: lattlong,
                zoom: 10,
                mapTypeControl: true,
                navigationControlOptions: {
                    style: google.maps.NavigationControlStyle.SMALL,
                },
            };
            var mapg = new google.maps.Map(
            document.getElementById("map"),
            OPTions
            );
            @foreach($location as $loc)
            latt = parseFloat(`{{$loc->m_location_geotag_x}}`);
            long = parseFloat(`{{$loc->m_location_geotag_y}}`);
            var lattlong = new google.maps.LatLng(latt,long);
            var markerg = new google.maps.Marker({
                position: lattlong,
                map: mapg,
                title: "You are here!",
            });
            @endforeach
        });
    </script>
</body>
</html>

@extends('template.app')

@section('nav-link/master/collapse')
nav-link collapsed
@endsection
@section('nav-link/master/aria-expanded')
false
@endsection

@section('nav-link/report/collapse')
nav-link collapsed
@endsection
@section('nav-link/report/aria-expanded')
false
@endsection

@section('page-title')
Fixed Asset - Home
@endsection

@section('nav-item/transaction/active')
active
@endsection

@section('nav-link/transaction/collapse')
nav-link
@endsection

@section('nav-link/transaction/aria-expanded')
true
@endsection

@section('collapse/transaction/show')
show
@endsection

@section('collapse-item/transaction/fixed_asset')
active bg-light
@endsection

@section('content')
<div class="container-fluid">
    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <!-- Card Header - Dropdown -->
        <div
        class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
        <h6 class="m-0 font-weight-bold text-primary">Fixed Assets Confirmation</h6>
        <div class="dropdown no-arrow">
            <a href="{{url('/fixed_asset/create')}}" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i
                class="fas fa-plus fa-sm text-white-50"></i> Add New</a>
            </div>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>Draft No.</th>
                            <th>Confrim No.</th>
                            <th>Confrim Date</th>
                            <!-- <th>Referensi No.</th> -->
                            <th>Material</th>
                            <th>Status</th>
                            <th>Note</th>
                            <th>FA</th>
                            <th>SJ</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $no = 1;
                        ?>
                        @foreach($asset as $a)
                        <tr>
                            <td>{{$no++}}</td>
                            <td>{{$a->t_asset_no}}</td>
                            <td>{{$a->t_asset_date}}</td>
                            <!-- <td>-</td> -->
                            <td>{{$a->code}}</td>
                            <td>{{$a->t_asset_h_status}}</td>
                            <td>{{$a->t_asset_h_note}}</td>
                            <td>{{($a->t_asset_dep_value/1)}}</td>
                            <td>{{$a->t_asset_dep_month}} Month</td>
                            <td>
                                <div class="row">
                                    <div class="col-md-5">
                                        <button class="btn-danger d-none d-sm-inline-block btn btn-sm shadow-sm mr-3" style="font-size: 20px;" title="hapus"><ion-icon name="trash-outline" onclick="del('{{ $a->t_asset_h_id }}')"></ion-icon></button>
                                    </div>
                                    <div class="col-md-5">
                                        <button class="btn-warning d-none d-sm-inline-block btn btn-sm shadow-sm mr-3" style="font-size: 20px;" title="Edit"><ion-icon name="create-outline" onclick="edit('{{ $a->t_asset_h_id }}','{{ ($no-1) }}')"></ion-icon></button>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<script type="module" src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.esm.js"></script>
<script nomodule src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.js"></script>
<script>
    function del($id){
        window.location.href="{{url('fixed_asset/delete')}}"+"/"+$id;
    }
    function edit($id,$no){
        window.location.href="{{url('fixed_asset/edit')}}"+"/"+$id+"/"+$no;
    }
</script>
@endsection

@extends('template.app')

@section('nav-link/master/collapse')
nav-link collapsed
@endsection
@section('nav-link/master/aria-expanded')
false
@endsection

@section('nav-link/report/collapse')
nav-link collapsed
@endsection
@section('nav-link/report/aria-expanded')
false
@endsection

@section('page-title')
Fixed Asset - Update
@endsection

@section('nav-item/transaction/active')
active
@endsection

@section('nav-link/transaction/collapse')
nav-link
@endsection

@section('nav-link/transaction/aria-expanded')
true
@endsection

@section('collapse/transaction/show')
show
@endsection

@section('collapse-item/transaction/fixed_asset')
active bg-light
@endsection

@section('content')
<div class="container-fluid">
    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <!-- Card Header - Dropdown -->
        <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
            <h6 class="m-0 font-weight-bold text-primary">Fixed Assets Confirmation</h6>
        </div>
        <div class="card-body">
            <form method="POST" action="{{url('/fixed_asset/update/'.$asset->t_asset_h_id)}}">
                @csrf
                <input type="hidden" name="curMonth" id="curMonth">
                <input type="hidden" name="curYear" id="curYear">
                <div class="row">
                    <div class="col-md-6">
                        <div class="row mb-3">
                            <label class="col-sm-4 col-form-label col-form-label-sm">Business Unit</label>
                            <div class="col-sm-8">
                                <select class="form-control form-control-sm" name="m_company_id">
                                    <option value="CV. ANUGRAH PRATAMA"
                                    @if($asset->m_company_id == 'CV. ANUGRAH PRATAMA')
                                    selected
                                    @endif
                                    >CV. ANUGRAH PRATAMA</option>
                                    <option value="CV. Surabaya Karya Jaya"
                                    @if($asset->m_company_id == 'CV. Surabaya Karya Jaya')
                                    selected
                                    @endif
                                    >CV. Surabaya Karya Jaya</option>
                                    <option value="CV. Surabaya Teknik"
                                    @if($asset->m_company_id == 'CV. Surabaya Teknik')
                                    selected
                                    @endif
                                    >CV. Surabaya Teknik</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="row mb-3">
                            <label class="col-sm-4 col-form-label col-form-label-sm">Draft No.</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control form-control-sm" value="{{ $no }}" readonly="" id="">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="row mb-3">
                            <label class="col-sm-4 col-form-label col-form-label-sm">Confrim No.</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control form-control-sm" id="" name="t_asset_no" value="{{ $asset->t_asset_no }}">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="row mb-3">
                            <label class="col-sm-4 col-form-label col-form-label-sm">Tanggal</label>
                            <div class="col-sm-4">
                                <input type="date" class="form-control form-control-sm" id="" name="t_asset_date" value="{{date('Y-m-d',strtotime($asset->t_asset_date))}}">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="row mb-3">
                            <label class="col-sm-4 col-form-label col-form-label-sm">Material</label>
                            <div class="col-sm-8">
                                <div class="input-group">
                                    <select class="form-control form-control-sm" aria-describedby="button-addon2" name="m_item_id">
                                        @foreach($barang as $bar)
                                        <option value="{{$bar->m_item_id}}"
                                            @if($asset->m_item_id == $bar->m_item_id)
                                            selected
                                            @endif
                                            >{{$bar->m_item_code}}</option>
                                            @endforeach
                                        </select>
                                        <button class="btn btn-outline-secondary btn-sm" type="button" id="button-addon2"><i class="fa fa-search"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="row mb-3">
                                <label class="col-sm-4 col-form-label col-form-label-sm">Divisi</label>
                                <div class="col-sm-8">
                                    <select class="form-control form-control-sm" name="m_divisi_id">
                                        <option value="DIVISI GENERAL"
                                        @if($asset->m_divisi_id == 'DIVISI GENERAL')
                                        selected
                                        @endif
                                        >DIVISI GENERAL</option>
                                        <option value="CABANG RIAU"
                                        @if($asset->m_divisi_id == 'CABANG RIAU')
                                        selected
                                        @endif
                                        >CABANG RIAU</option>
                                        <option value="CABANG BONDOWOSO"
                                        @if($asset->m_divisi_id == 'CABANG BONDOWOSO')
                                        selected
                                        @endif
                                        >CABANG BONDOWOSO</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="row mb-3">
                                <label class="col-sm-4 col-form-label col-form-label-sm">Qty</label>
                                <div class="col-sm-4">
                                    <input type="number" class="form-control form-control-sm" id="t_asset_qty" name="t_asset_qty" value="{{$asset->t_asset_qty/1}}">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="row mb-3">
                                <label class="col-sm-4 col-form-label col-form-label-sm">Asset Value</label>
                                <div class="col-sm-4">
                                    <input type="number" class="form-control form-control-sm" id="t_asset_price" name="t_asset_price" value="{{$asset->t_asset_price/1}}">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="row mb-3">
                                <label class="col-sm-4 col-form-label col-form-label-sm">Asset Account</label>
                                <div class="col-sm-8">
                                    <select class="form-control form-control-sm" name="t_asset_account_id">
                                        <option value="Tanah"
                                        @if($asset->t_asset_account_id == 'Tanah')
                                        selected
                                        @endif
                                        >Tanah</option>
                                        <option value="Bangunan"
                                        @if($asset->t_asset_account_id == 'Bangunan')
                                        selected
                                        @endif
                                        >Bangunan</option>
                                        <option value="Kendaraan"
                                        @if($asset->t_asset_account_id == 'Kendaraan')
                                        selected
                                        @endif
                                        >Kendaraan</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="row mb-3">
                                <label class="col-sm-4 col-form-label col-form-label-sm">Accum. Dept. Account</label>
                                <div class="col-sm-8">
                                    <select class="form-control form-control-sm" name="t_asset_account_dep_cost_id">
                                        <option value="Akumulasi Depr Tanah"
                                        @if($asset->t_asset_account_dep_cost_id == 'Akumulasi Depr Tanah')
                                        selected
                                        @endif
                                        >Akumulasi Depr Tanah</option>
                                        <option value="Akumulasi Depr Bangunan"
                                        @if($asset->t_asset_account_dep_cost_id == 'Akumulasi Depr Bangunan')
                                        selected
                                        @endif
                                        >Akumulasi Depr Bangunan</option>
                                        <option value="Akumulasi Depr Kendaraan"
                                        @if($asset->t_asset_account_dep_cost_id == 'Akumulasi Depr Kendaraan')
                                        selected
                                        @endif
                                        >Akumulasi Depr Kendaraan</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="row mb-3">
                                <label class="col-sm-4 col-form-label col-form-label-sm">Depreciation</label>
                                <div class="col-sm-6">
                                    <div class="input-group">
                                        <input type="number" class="form-control form-control-sm" placeholder="" aria-describedby="button-addon2" id="t_asset_dep_month" name="t_asset_dep_month" value="{{$asset->t_asset_dep_month}}">
                                        <button class="btn btn-outline-secondary btn-sm" disabled="" type="button" id="button-addon2">Month</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="row mb-3">
                                <label class="col-sm-4 col-form-label col-form-label-sm">Depreciation Account</label>
                                <div class="col-sm-8">
                                    <select class="form-control form-control-sm" name="t_asset_account_dep_id">
                                        <option value="Biaya Depr Tanah"
                                        @if($asset->t_asset_account_dep_id == 'Biaya Depr Tanah')
                                        selected
                                        @endif
                                        >Biaya Depr Tanah</option>
                                        <option value="Biaya Depr Bangunan"
                                        @if($asset->t_asset_account_dep_id == 'Biaya Depr Bangunan')
                                        selected
                                        @endif
                                        >Biaya Depr Bangunan</option>
                                        <option value="Biaya Depr Kendaraan"
                                        @if($asset->t_asset_account_dep_id == 'Biaya Depr Kendaraan')
                                        selected
                                        @endif
                                        >Biaya Depr Kendaraan</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="row mb-3">
                                <label class="col-sm-4 col-form-label col-form-label-sm">Depreciation Value</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control form-control-sm" id="t_asset_dep_value" readonly="readonly" name="t_asset_dep_value" value="{{($asset->t_asset_dep_value-0)/($asset->t_asset_dep_month-0)}}">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="row mb-3">
                                <label class="col-sm-4 col-form-label col-form-label-sm">Status</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control form-control-sm" id="" name="t_asset_h_status" readonly="readonly" value="{{$asset->t_asset_h_status}}">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="row mb-3">
                                <label class="col-sm-4 col-form-label col-form-label-sm">Header Note</label>
                                <div class="col-sm-8">
                                    <textarea type="text" class="form-control form-control-sm" id="" name="t_asset_h_note">{{$asset->t_asset_h_note}}</textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row my-4">
                        <div class="col-md-12">
                            <h6 class="m-0 font-weight-bold text-primary">Fixed Assets Confirmation Detail</h6>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="float-right">
                                <div id="generate" onclick="generate()">
                                    <button type="button" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm mb-2">
                                        Generate Detail Data</button>
                                    </div>
                                    <div id="reset" onclick="res()">
                                        <button type="button" class="d-none d-sm-inline-block btn btn-sm btn-secondary shadow-sm mb-2">
                                            Reset Detail Data</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="table-responsive">
                                        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                            <thead>
                                                <tr>
                                                    <th>No.</th>
                                                    <th>Period</th>
                                                    <th>Value</th>
                                                    <th>Depreciation</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                $monthname = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
                                                ?>
                                                @foreach($detail as $det)
                                                <tr>
                                                    <td>{{$det->t_asset_d_seq}}</td>
                                                    <td>{{$monthname[$det->t_asset_d_month-1]}} {{$det->t_asset_d_year}}</td>
                                                    <td>{{$det->t_asset_d_value/1}}</td>
                                                    <td>{{$det->t_asset_d_accum/1}}</td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="row float-right">
                                <button type="submit" class="btn-success d-none d-sm-inline-block btn btn-md shadow-sm mr-3">Simpan</button>
                                <a href="#" class="btn-warning d-none d-sm-inline-block btn btn-md shadow-sm mr-3">Terbitkan</a>
                                <a href="{{url('fixed_asset')}}" class="btn-light d-none d-sm-inline-block btn btn-md shadow-sm">Cancel</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
<script>
    $(document).ready(function () {
        $('#generate').hide();
        $('#curMonth').val('{{$asset->created_at}}'.substr(5, 2)-1);
        $('#curYear').val('{{$asset->created_at}}'.substr(0, 4)-0);
    });
    function number(){
        var qty = $('#t_asset_qty').val();
        var val = $('#t_asset_price').val();
        var dep = $('#t_asset_dep_month').val();
        if (qty > 0 && val > 0 && dep > 0) {
            var price = (qty*val)/dep;
            $('#t_asset_dep_value').val(price.toFixed(2));
        }else{
            $('#t_asset_dep_value').val(0);
        }
    }
    $('#t_asset_qty').on('input',function(e){
        number();
    });
    $('#t_asset_price').on('input',function(e){
        number();
    });
    $('#t_asset_dep_month').on('input',function(e){
        number();
    });
    function res(){
        $('#reset').hide();
        $('#generate').show();
        $("table tbody tr").remove();
    };
    function generate(){
        var qty = $('#t_asset_qty').val();
        var val = $('#t_asset_price').val();
        var dep = $('#t_asset_dep_month').val();
        var depval = $('#t_asset_dep_value').val();
        var maxval = qty * val;
        if (depval > 0) {
            $('#generate').hide();
            $('#reset').show();
            $("table tbody tr").remove();
            var currentMonth = `{{$asset->created_at}}`.substr(5, 2)-1;
            var currentYear = '{{$asset->created_at}}'.substr(0, 4)-0;
            const monthNames = ["January", "February", "March", "April", "May", "June",
            "July", "August", "September", "October", "November", "December"
            ];
            tableBody = $("table tbody");
            for(var i = 0;i<dep;i++){
                if (i < dep-1) {
                    maxval = maxval - depval;
                    var markup = '';
                    markup += `<tr>`;
                    markup += `<td>`;
                    markup += (i+1);
                    markup += `</td>`;
                    markup += `<td>`;
                    markup += monthNames[currentMonth]+" "+currentYear;
                    markup += `</td>`;
                    markup += `<td>`;
                    markup += depval;
                    markup += `</td>`;
                    markup += `<td>`;
                    markup += maxval.toFixed(2);
                    markup += `</td>`;
                    markup += `</tr>`;

                    tableBody.append(markup);
                    if (currentMonth + 1 == 12) {
                        currentMonth = 0;
                        currentYear = currentYear + 1;
                    }else{
                        currentMonth = currentMonth + 1;
                    }
                }else{
                    var markup = '';
                    markup += `<tr>`;
                    markup += `<td>`;
                    markup += (i+1);
                    markup += `</td>`;
                    markup += `<td>`;
                    markup += monthNames[currentMonth]+" "+currentYear;
                    markup += `</td>`;
                    markup += `<td>`;
                    markup += depval;
                    markup += `</td>`;
                    markup += `<td>`;
                    markup += "0";
                    markup += `</td>`;
                    markup += `</tr>`;

                    tableBody.append(markup);
                }
            }
        }
    };
</script>
@endsection
